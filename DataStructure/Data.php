<?php
namespace P3\DataStructure;

/**
 * @author Samuel Carlier
 * @entity
 */
class Data {

	/**
	 * @primary
	 */
	protected $id;

	/**
	 * @collection
	 */
	protected $collectionName = '';
	
	/**
	 * @array
	 */
	protected $values = array();

	/**
	 * @string
	 */
	protected $structurePrimary;
	
	/**
	 * @date
	 */
	protected $insertDate;

	/**
	 * @param string $structurePrimaryHash
	 * @param string $collectionName
	 */
	public function __construct($structurePrimaryHash=null, $collectionName=null) {
		if(!$this->insertDate) {
			$this->insertDate = new \DateTime;
		}

		if($structurePrimaryHash) {
			$this->setStructurePrimary($structurePrimaryHash);
		}
	
		if($collectionName) {
			$this->setCollectionName($collectionName);
		}
	}

	/**
	 * @return \DateTime
	 */
	public function getInsertDate() {
		return $this->insertDate;
	}

	/**
	 * @param  string $structurePrimaryHash
	 * @return self
	 */
	public function setStructurePrimary($structurePrimaryHash) {
		if(!is_string($structurePrimaryHash)) {
			throw Exception::invalidId();
		}
		
		$this->structurePrimary = $structurePrimaryHash;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStructurePrimary() {
		return $this->structurePrimary;
	}
	
	/**
	 * @param string $itemId
	 * @param mixed  $value
	 * @param string $language
	 * 
	 * @return self
	 */
	public function addValue($itemId, $value, $language="default") {
		if(!is_string($itemId)) {
			throw Exception::invalidId();
		}
		
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		//check for duplicate itemId
		$find = $this->getValue($itemId, $language);
		if($find !== false) {
			throw Exception::duplicateDataItem($itemId);
		}
		
		$this->values[] = array('itemId'   => $itemId,
								'value'	   => $value,
								'language' => $language
		);

		return $this;
	}
	
	/**
	 * @param  string $language
	 * @return self
	 */
	public function removeValuesFromLanguage($language="default") {
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		foreach($this->values as $key => $value) {
			if($value['language'] == $language) {
				unset($this->values[$key]);
			}
		}
		
		//clean up index
		$refresh = array();
		foreach($this->values as $key => $value) {
			$refresh[] = $value;
		}		
		
		$this->values = $refresh;

		return $this;
	}
	
	/**
	 * @param string $itemId
	 * @param string $language
	 *
	 * @return self
	 */
	public function removeValue($itemId, $language="default") {
		if(!is_string($itemId)) {
			throw Exception::invalidId();
		}
		
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		foreach($this->values as $key => $value) {
			if($value['itemId'] == $itemId && $value['language'] == $language) {
				unset($this->values[$key]);
			}
		}
		
		//cleaup index
		$refresh = array();
		foreach($this->values as $key => $value) {
			$refresh[] = $value;
		}		
		
		$this->values = $refresh;
		return $this;
	}
	
	/**
	 * @param string $itemId
	 * @param string $language
	 *
	 * @return array
	 * 		   false if not found
	 */
	public function getValue($itemId, $language="default") {
		if(!is_string($itemId)) {
			throw Exception::invalidId();
		}
		
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		foreach($this->values as $value) {
			if($value['itemId'] == $itemId && $value['language'] == $language) {
				return $value;
			}
		}
		
		return false;
	}
	
	/**
	 * @return self
	 */
	public function clearValues() {
		$this->values = array();
		return $this;
	}

	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param string $name
	 * @return self
	 */
	public function setCollectionName($name) {
		if(!is_string($name)) {
			throw Exception::invalidCollectionName();
		}
		
		$this->collectionName = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCollectionName() {
		return $this->collectionName;
	}
	
}
