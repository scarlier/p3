<?php
namespace P3\DataStructure;

/**
 * @author Samuel Carlier
 * @entity 
 */
class Structure {

	/**
	 * @primary
	 */
	protected $id;

	/**
	 * @string
	 */
	protected $name;
	
	/**
	 * @string
	 */
	protected $type;
	
	/**
	 * @array
	 */
	protected $label = array();
	
	/**
	 * @array
	 */
	protected $description = array();

	/**
	 * @embed
	 */
	protected $items = array();
	
	/**
	 * @collection
	 */
	protected $collectionName = '';

	/**
	 * @date
	 */
	protected $insertDate;

	/**
	 * 
	 */
	public function __construct() {
		if(!$this->insertDate) {
			$this->insertDate = new \DateTime;
		}
	}
	
	/**
	 * @param string $type
	 * @return self
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	

	/**
	 * @param string $name
	 * @return self
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * @param string $label
	 * @param string $language
	 * 
	 * @return self
	 */
	public function setLabel($label, $language="default") {
		$this->label[$language] = $label;
		return $this;
	}
	
	/**
	 * @param  string $language
	 * @return string
	 */
	public function getLabel($language="default") {
		if(!array_key_exists($language, $this->label)) {
			return '';
		}

		return $this->label[$language];
	}
	
	/**
	 * @param string $description
	 * @param string $language
	 * 
	 * @return self
	 */
	public function setDescription($description, $language="default") {
		$this->description[$language] = $description;
		return $this;
	}
	
	/**
	 * @param  string $language
	 * @return string
	 */
	public function getDescription($language="default") {
		if(!array_key_exists($language, $this->description)) {
			return '';
		}

		return $this->description[$language];
	}
	
	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param string $name
	 * @return self
	 */
	public function setCollectionName($name) {
		if(!is_string($name)) {
			throw Exception::invalidCollectionName();
		}
		
		$this->collectionName = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCollectionName() {
		return $this->collectionName;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getInsertDate() {
		return $this->insertDate;
	}
	
	/**
	 * @param \P3\DataStructure\Item $item
	 * @return self
	 */
	public function addItem(\P3\DataStructure\Item $item) {
		$this->items[] = $item;
		return $this;
	}

	/**
	 * @param  string $id
	 * @return self
	 */
	public function removeItem($id) {
		$itemKey = $this->findItemById($id);
		if($itemKey) {
			unset($this->items[$itemKey]);
		}
		
		return $this;
	}
	
	/**
	 * @param string $id
	 * 
	 * @return \P3\DataStructure\Item
	 * 		   false if not found
	 */
	public function getItemById($id) {
		$itemKey = $this->findItemById($id);
		if(!$itemKey) {
			return false;
		}
		
		return $this->items[$itemKey];
	}
	
	/**
	 * position start at 0
	 * 
	 * @param int $pos
	 * @return \P3\DataStructure\Item
	 * 		   false if not found
	 */
	public function getItemByPosition($pos) {
		if(array_key_exists($pos, $this->items)) {
			return $this->items[$pos];
		}
		
		return false;
	}
	
	/**
	 * @return self
	 */
	public function clearItems() {
		$this->items = array();
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * @param  string $id
	 * @return int 
	 * 		   false if not found
	 */
	private function findItemById($id) {
		foreach($this->items as $k => $item) {
			if($item->getId() == $id) {
				return $item;
			}
		}
		
		return false;
	}
}
