<?php

namespace P3\DataStructure;

/**
 * @author Samuel Carlier
 */
class Exception extends \Exception {

	/**
	 * @return self
	 */
	public static function invalidCollectionName() {
		return new self("CollectionName should be of type string");
	}

	/**
	 * @return self
	 */
	public static function invalidId() {
		return new self("id should be of type string");
	}

	/**
	 * @return self
	 */
	public static function invalidLanguage() {
		return new self("language should be of type string");
	}

	/**
	 * @return self
	 */
	public static function duplicateDataItem($id) {
		return new self("item with id '$id' already exists in data object");
	}
}