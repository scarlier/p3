<?php
namespace P3\DataStructure;

/**
 * @author Samuel Carlier
 * @entity
 */
class Item {

	/**
	 * @string
	 */
	protected $id;

	/**
	 * @bool
	 */
	protected $isRequired;

	/**
	 * @string 
	 */
	protected $name;

	/**
	 * @array 
	 */
	protected $label = array();

	/**
	 * @array 
	 */
	protected $description = array();

	/**
	 * 
	 */
	public final function __construct() {
		if(!$this->id) {
			$this->id = $this->generateUniqId();
		}
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * @param string $name
	 * @return self
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @param  string $language
	 * @return string
	 */
	public function getLabel($language="default") {
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		if(!array_key_exists($language, $this->label)) {
			return '';
		}
		
		return $this->label[$language];
	}
	
	/**
	 * @param  string $label
	 * @param  string $language
	 * 
	 * @return self
	 */
	public function setLabel($label, $language="default") {
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		$this->label[$language] = $label;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription($language="default") {
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
		
		if(!array_key_exists($language, $this->description)) {
			return '';
		}

		return $this->description[$language];
	}
	
	/**
	 * @param  string $description
	 * @param  string $language
	 * 
	 * @return self
	 */
	public function setDescription($description, $language="default") {
		if(!is_string($language)) {
			throw Exception::invalidLanguage();
		}
	
		$this->description[$language] = $description;
		return $this;
	}
	
	/**
	 * @param  bool $required
	 * @return bool
	 */
	public function isRequired($required=null) {
		if($required !== null) {
			$this->isRequired = !empty($required);
		}

		return $this->isRequired;
	}
	
	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return get_class($this);
	}

	/**
	 * returns a uuid4 string
	 * 
	 * @return string
	 */
	private function generateUniqId() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);

	}
}