<?php
namespace P3\DataStructure\Item;

/**
 * @author Samuel Carlier
 * @entity
 */
class Text extends \P3\DataStructure\Item {
	
	/**
	 * @int
	 */
	protected $maxLenght;
	
	/**
	 * @bool
	 */
	protected $isMultiLine = false;
	
	/**
	 * @bool
	 */
	protected $isWYSIWYG = false;
	
	/**
	 * @param  bool $boolean
	 * @return bool
	 */
	public function isMultiLine($boolean=null) {
		if($boolean !== null) {
			$this->isMultiLine = !empty($boolean);
		}

		return $this->isMultiLine;
	}
	
	/**
	 * @param  bool $boolean
	 * @return bool
	 */
	public function isWYSIWYG($boolean=null) {
		if($boolean !== null) {
			$this->isWYSIWYG = !empty($boolean);
		}

		return $this->isWYSIWYG;
	}
	
	/**
	 * @return int
	 */
	public function getMaxLenght() {
		return $this->maxLenght;
	}
	
	/**
	 * @param int $maxLenght
	 * @return self
	 */
	public function setMaxLenght($maxLenght) {
		$this->maxLenght = $maxLenght;
		return $this;
	}
}