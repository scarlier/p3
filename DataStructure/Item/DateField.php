<?php
namespace P3\DataStructure\Item;

/**
 * @author Samuel Carlier
 * @entity
 */
class DateField extends \P3\DataStructure\Item {
	
	/**
	 * validate if the string is a proper date string
	 *
	 * @param string $string
	 */
	public function validate($string) {
		if($string === '' || $string === null) {
			return true;
		}

		$date = strtotime($string);

		if(date("d-m-Y", $date) == $string) {
			return true;
		}

		return false;
	}
}