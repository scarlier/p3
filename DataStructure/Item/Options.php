<?php
namespace P3\DataStructure\Item;

/**
 * @author Samuel Carlier
 * @entity
 */
class Options extends \P3\DataStructure\Item {

	/**
	 * @array
	 */
	protected $options = array();

	/**
	 * @param  string $language
	 * @return array
	 */
	public function getOptions($language="default") {
		$options = array();
		foreach($this->options as $option) {
			if($option['language'] == $language) {
				$options[] = $option;
			}
		}

		usort($options, function($a, $b) {
			return strcmp($a['value'], $b['value']);
		});
		
		return $options;
	}

	/**
	 * @param  string $key
	 * @param  string $language
	 * 
	 * @return array
	 * 		   false if not found
	 */
	public function getOption($key, $language="default") {
		$options = array();
		foreach($this->options as $option) {
			if($option['language'] == $language && $option['key'] == $key) {
				return $option;
			}
		}

		return $false;
	}
	
	/**
	 * @param  string $key
	 * @param  string $value
	 * @param  string $language
	 * 
	 * @return self
	 */
	public function addOption($key, $value, $language="default") {
		$this->options[] = array('key' => $key, 'value' => $value, 'language' => $language);
		return $this;
	}

	/**
	 * @param string $key
	 * @param string $language
	 * 
	 * @return self
	 */
	public function removeOption($key, $language="default") {
		foreach($this->options as $k => $option) {
			if($option['language'] == $language && $option['key'] == $key) {
				unset($this->options[$k]);
			}
		}
		
		//refresh option index
		$refresh = array();
		foreach($this->options as $k => $option) {
			$refresh[] = $option;
		}		

		$this->options = $refresh;
		return $this;
	}
	
	/**
	 * @return self
	 */
	public function clearOptions() {
		$this->options = array();
		return $this;
	}
}
