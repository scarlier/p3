<?php
namespace P3\Mongo\Hydrate;

/**
 * @author Samuel Carlier
 */
class Object {

	/**
	 * @param array $embeded_data
	 * @param \P3\Mongo\DB $db
	 */
	private static function parseEmbedDocument($embeded_data, \P3\Mongo\DB $db) {
		if(!is_array($embeded_data)) return;
		list($class, $properties) = each($embeded_data);
		$embed_document_object = new $class;

		$reflection = new \P3\Mongo\Reflection\ReflectionClass($embed_document_object);
		if(!$reflection->isEntity()) {
			throw \P3\Mongo\Exception::entityNotExists($reflection->getName());
		}

		foreach($reflection->getProperties() as $property) {
			if(!array_key_exists($property->getName(), $properties)) {
				continue;
			}

			$value = self::parseProperty($property, $properties[$property->getName()], $db);
			$property->setValue($embed_document_object, $value);
		}

		return $embed_document_object;
	}

	/**
	 * @param \P3\Mongo\Reflection\Property $property
	 * @param mixed $data
	 * @param \P3\Mongo\DB $db
	 */
	private static function parseProperty(\P3\Mongo\Reflection\Property $property, $data, \P3\Mongo\DB $db) {

		if($property->isReference()) {
			$data = empty($data) ? array() : $data;
			$data = new \P3\Mongo\Iterator\Reference($data, $db);
		}

		else if($property->isEmbed() && !empty($data)) {

			//multi embedded documents are starting with a 0 index
			if(!empty($data[0])) {
				foreach($data as $key => $embeded_data) {
					$data[$key] = self::parseEmbedDocument($embeded_data, $db);
				}

			} else {
				$data = self::parseEmbedDocument($data, $db);
			}
		}

		else if($property->isDate() && $data instanceof \MongoDate) {
			if($data->sec) {
				$seconds = (string) $data->sec;

				$data = new \DateTime;
				$data->setTimestamp((int) $seconds);

			} else {
				$data = null;
			}
		}

		return $data;
	}

	/**
	 * @param array $data
	 * @param string $objectName
	 * @param \P3\Mongo\DB $db
	 * @param string $collectionName
	 *
	 * @return null   if mapped class is not a entity
	 *		   object if data mapping to object is successful
	 */
	public static function perform($data, $objectName, \P3\Mongo\DB $db, $collectionName) {
		if(!is_array($data)) {
			return null;
		}
		$reflection = new \P3\Mongo\Reflection\ReflectionClass($objectName);
		if(!$reflection->isEntity()) {
			throw \P3\Mongo\Exception::entityNotExists($reflection->getName());
		}

		$object = new $objectName;

		foreach($reflection->getProperties() as $property) {
			$name = $property->getName();

			//collectionName propery is never saved in the database but it needs to be filled
			if($property->isCollectionName()) {
				$data[$name] = $collectionName;
			}
			
			if(!isset($data[$name])) {
				continue;
			}

			$value = self::parseProperty($property, $data[$name], $db);
			$property->setValue($object, $value);
		}

		if(!empty($data['_id'])) {
			$primary = $reflection->getPrimary();
			if(empty($primary)) {
				throw \P3\Mongo\Exception::missingPrimaryProperty($reflection->getName());
			}

			$primary->setValue($object, (string) $data['_id']);
		}

		return $object;
	}
}