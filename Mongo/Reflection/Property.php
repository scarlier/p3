<?php
namespace P3\Mongo\Reflection;

/**
 * @author Samuel Carlier
 */
class Property {

	/**
	 * @var ReflectionProperty
	 */
	private $reflection;

	/**
	 * @var string
	 */
	private $name;
	
	/**
	 * @var array
	 */
	private $cache = array();

	/**
	 * @var array
	 */
	private $parsed = array();

	/**
	 * @param ReflectionProperty $property
	 * @param array $cache
	 */
	public function __construct(\ReflectionProperty $property, array $cache = array()) {
		$this->reflection = $property;
		
		$this->name = $property->getName();
		$this->cache = $cache;

		if(!isset($this->cache['docblock'])) {
			$this->parseDocBlock();
	
		} else {
			$this->parsed = $this->cache['docblock'];
		}
	}
	
	/**
	 * @return array
	 */
	public function getParsedDocBlock() {
		return $this->parsed;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isPrimary() {
		return $this->primary !== null;
	}

	/**
	 * @return bool
	 */
	public function isString() {
		return $this->string !== null;
	}

	/**
	 * @return bool
	 */
	public function isArray() {
		return $this->array !== null;
	}

	/**
	 * @return bool
	 */
	public function isDate() {
		return $this->date !== null;
	}

	/**
	 * @return bool
	 */
	public function isInteger() {
		return ($this->integer !== null || $this->int !== null);
	}

	/**
	 * @return bool
	 */
	public function isBoolean() {
		return ($this->bool !== null || $this->boolean !== null);
	}

	/**
	 * @return bool
	 */
	public function isReference() {
		return $this->reference !== null;
	}
	
	/**
	 * @return bool
	 */
	public function isCollectionName() {
		return $this->collection !== null;
	}


	/**
	 * @return bool
	 */
	public function isEmbed() {
		return $this->embed !== null;
	}

	/**
	 * @return bool
	 */
	public function hasType() {
		return $this->isArray() || $this->isString() || $this->isInteger() || $this->isEmbed() || $this->isReference() || $this->isDate() || $this->isBoolean();
	}

	/**
	 * @param  string $name
	 * @return string
	 */
	public function __get($name) {
		if(array_key_exists($name, $this->parsed)) {
			return $this->parsed[$name];
		}

		return null;
	}

	/**
	 * @param mided $object
	 * @param mixed	$value
	 */
	public function setValue($object, $value) {
		$this->reflection->setValue($object, $value);
	}

	/**
	 * @param  mixed $object
	 * @return mixed
	 */
	public function getValue($object) {
		return $this->reflection->getValue($object);
	}

	/**
	 * 
	 */
	private function parseDocBlock() {
		$doc = trim(trim($this->reflection->getDocComment()), '/');
		$doc = trim(str_replace('*', '', $doc));
		$doc = explode('@', $doc);

		if(empty($doc)) {
			return;
		}

		foreach($doc as $line) {
			$line = preg_replace('/\s+/', ' ', $line);
			if(empty($line)) {
				continue;
			}

			$partEnd = strpos($line, ' ');

			$key   = $partEnd === false ? $line : substr($line, 0, $partEnd);
			$value = $partEnd === false ? true  : substr($line, $partEnd + 1);

			$this->parsed[trim($key)] = trim($value);
		}

		return;
	}
}
