<?php
namespace P3\Mongo\Reflection;

/**
 * @author Samuel Carlier
 */
class ReflectionClass {

	/**
	 * @var array 
	 */
	static $_cacheClass = array();
	
	/**
	 *
	 * @var array
	 */
	private $loadedClassCache = array();
	
	/**
	 * @var \ReflectionClass
	 */
	private $reflection;
	
	/**
	 * @var Object
	 */
	private $objectReflected;
	
	/**
	 * @var string
	 */
	private $name;
	
	/**
	 * @var array
	 */
	private $properties = array();

	/**
	 * @var array
	 */
	private $parsed = array();
	
	/**
	 * 
	 */
	private function saveReflectionSnapshot() {
		if($this->loadedClassCache) {
			return;
		}

		$file = MARCOMPRO_ROOT . 'public/tmp/' . md5($this->name) . '.cache.txt';
		
		$data = array(
			'docblock' => $this->parsed,
			'isEntity' => $this->isEntity()
		);
		
		$data['properties'] = array();
		foreach($this->properties as $property) {
			$data['properties'][$property->getName()]['docblock'] = $property->getParsedDocBlock();
		}

		file_put_contents($file, serialize($data));
	}

	/**
	 * 
	 */
	private function loadReflectionSnapshot() {
		if(isset(self::$_cacheClass[$this->name])) {
			$this->loadedClassCache = self::$_cacheClass[$this->name];
			return;
		}

		$file = MARCOMPRO_ROOT . 'public/tmp/' . md5($this->name) . '.cache.txt';
		if(file_exists($file)) {
			self::$_cacheClass[$this->name] = unserialize(file_get_contents($file));
			$this->loadedClassCache = self::$_cacheClass[$this->name];
		}
	}
	
	/**
	 * @param mixed $class
	 */
	public function __construct($class) {
		$this->objectReflected = is_object($class) ? $class : null;

		$this->reflection = is_object($class) ? new \ReflectionObject($class) : new \ReflectionClass($class);
		$this->name		  = $this->reflection->getName();

		if(\P3\Mongo\Manager::isCacheEnabled()) {
			$this->loadReflectionSnapshot();
		}

		foreach($this->reflection->getProperties() as $property) {			
			$property->setAccessible(true);

			$propertyCache = isset($this->loadedClassCache['properties'][$property->getName()]) ? $this->loadedClassCache['properties'][$property->getName()] : array();
			$this->properties[] = new Property($property, $propertyCache);
		}
		
		if(!$this->loadedClassCache || !isset($this->loadedClassCache['docblock'])) {
			$this->parseDocBlock();

		} else {
			$this->parsed = $this->loadedClassCache['docblock'];
		}
		
		if(\P3\Mongo\Manager::isCacheEnabled()) {
			$this->saveReflectionSnapshot();
		}
	}

	/**
	 * @return string
	 */
	public function getCollectionName() {
		//first check if there is a collection name in a attribute
		$collectionname = '';
		
		if($this->objectReflected) {
			foreach($this->properties as $property) {
				if($property->isCollectionName()) {
					$collectionname = $property->getValue($this->objectReflected);
					break;
				}
			}
		}
		
		if(!empty($collectionname)) {
			return $collectionname;
		}
		
		//check if there is a collectionname in a docblock header
		if(!empty($this->parsed['entity']['collection'])) {
			return $this->parsed['entity']['collection'];
		}

		//no collection name found use classname
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isEntity() {
		if(isset($this->loadedClassCache['isEntity'])) {
			return $this->loadedClassCache['isEntity'];
		}
		
		$doc = $this->reflection->getDocComment();
		return strpos($doc, '@entity') !== false;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return \P3\Mongo\Reflection\Property
	 */
	public function getPrimary() {
		foreach($this->properties as $property) {
			if(!$property->isPrimary()) {
				continue;
			}

			return $property;
		}
	}

	/**
	 * @return array
	 */
	public function getProperties() {
		return $this->properties;
	}

	/**
	 *
	 */
	private function parseDocBlock() {
		$doc = trim(trim($this->reflection->getDocComment()), '/');
		$doc = trim(str_replace('*', '', $doc));
		$doc = explode('@', $doc);

		if(empty($doc)) {
			return;
		}

		foreach($doc as $line) {
			$line = preg_replace('/\s+/', ' ', $line);
			if(empty($line)) {
				continue;
			}

			$partEnd = strpos($line, ' ');

			$key   = $partEnd === false ? $line : substr($line, 0, $partEnd);
			$value = $partEnd === false ? true  : trim(substr($line, $partEnd + 1));

			$config = $value;
			if(strpos($value, '=') !== false) {
				$value  = explode(',', $value);
				$config = array();

				foreach($value as $parts) {
					$parts = explode('=', $parts);
					$config[trim($parts[0])] = trim($parts[1]);
				}
			}

			$this->parsed[trim($key)] = $config;
		}

		return;
	}
}