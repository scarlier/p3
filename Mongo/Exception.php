<?php
namespace P3\Mongo;

/**
 * @author  Samuel Carlier
 * @package P3\Mongo
 */
class Exception extends \Exception {

	/**
	 * @param string $name
	 * @return \P3\Mongo\Exception
	 */
	public static function entityNotExists($name) {
		return new self('object (' . $name . ')  mapped to data is not a entity');
	}

	/**
	 * @param string $entity
	 * @param string $property
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function dateFieldTypeError($entity, $property) {
		return new self('property ' . $property . ' of entity' . $entity . ' must be \DateTime Type');
	}

	/**
	 * @param string $name
	 * @return \P3\Mongo\Exception
	 */
	public static function missingPrimaryProperty($name) {
		return new self('entity "'.$name.'" missing primary property');
	}

	/**
	 * @param int $position
	 * @return \P3\Mongo\Exception
	 */
	public static function invalidSeek($position) {
		return new self('invalid seek position (' . $position . ')');
	}

	/**
	 * @param  string $method
	 * @return \P3\Mongo\Exception
	 */
	public static function invalidObject($method) {
		return new self('argument passed in ' . $method . ' was not a object');
	}
	
	/**
	 * @param string $name
	 * @param string $method
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function notString($name, $method) {
		return new self('argument ' . $name . ' on ' . $method . ' was empty or not a string');
	}

	/**
	 * @param string $name
	 * @param string $method
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function authFailed($msg, $method) {
		return new self($msg . ' on ' . $method);
	}

	/**
	 * @param string $database
	 * @param array  $msg
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function dropDBFailed($database, $msg) {
		return new self('drop database "' . $database. '" failed ' . print_r($msg, true));
	}

	/**
	 * @param string $collection
	 * @param array  $msg
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function dropCollectionFailed($collection, $msg) {
		return new self('drop collection "' . $collection. '" failed ' . print_r($msg, true));
	}

	/**
	 * @param string $collection
	 * @param array  $msg
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function deleteIndexesFailed($collection, $msg) {
		return new self('delete indexes failed in collection "' . $collection. '" ' . print_r($msg, true));
	}

	/**
	 * @param string $database
	 * @param array  $msg
	 *
	 * @return \P3\Mongo\Exception
	 */
	public static function repairFailed($database, $msg) {
		return new self('repair database "' . $database. '" failed ' . print_r($msg, true));
	}

	/**
	 * @return \P3\Mongo\Exception
	 */
	public static function invalidReference() {
		return new self('given reference object is not valid');
	}

	/**
	 * @return \P3\Mongo\Exception
	 */
	public static function referenceMissingPrimary() {
		return new self('given reference object does not have a primary key/value');
	}
}