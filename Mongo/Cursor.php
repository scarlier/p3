<?php
namespace P3\Mongo;

/**
 * @author Samuel Carlier
 */
class Cursor implements \Iterator {

	/**
	 *
	 */
//	const MAX_ERROR_TRY = 4;

	/**
	 * @var int
	 */
//	private $errorCount = 0;

	/**
	 * @var \MongoCursor
	 */
	private $cursor;

	/**
	 * @var string
	 */
	private $collectionName;
	
	/**
	 * @var string
	 */
	private $objectName;

	/**
	 * @var \P3\Mongo\DB
	 */
	private $db;

	/**
	 * @param \MongoCursor $cursor
	 * @param \P3\Mongo\DB $db
	 * @param string	   $objectName
	 * @param string	   $collectionName
	 */
	public function __construct(\MongoCursor $cursor, \P3\Mongo\DB $db, $objectName, $collectionName) {
		$this->cursor = $cursor;
		$this->db 	  = $db;
		
		$this->collectionName = $collectionName;
		$this->objectName     = $objectName;
	}

	/**
	 * Sets a client-side timeout for this query
	 *
	 * @param int $ms
	 * @return \P3\Mongo\Cursor
	 */
	public function timeout($ms) {
		$this->cursor->timeout($ms);
		return $this;
	}

	/**
	 * Sets whether this cursor will be left open after fetching the last results
	 *
	 * @param bool $tail If the cursor should be tailable.
	 * @return \P3\Mongo\Cursor
	 */
	public function tailable($tail=true) {
		$this->cursor->tailable($tail);
		return $this;
	}

	/**
	 * Sorts the results by given fields
	 *
	 * @param array $fields
	 * @return \P3\Mongo\Cursor
	 */
	public function sort(array $fields) {
		$this->cursor->sort($fields);
		return $this;
	}

	/**
	 * Use snapshot mode for the query
	 *
	 * @return \P3\Mongo\Cursor
	 */
	public function snapshot() {
		$this->cursor->snapshot();
		return $this;
	}

	/**
	 * Sets whether this query can be done on a slave
	 *
	 * @param bool $okay If it is okay to query the slave. 
	 * @return \P3\Mongo\Cursor
	 */
	public function slaveOkay($okay=true) {
		$this->cursor->slaveOkay($okay);
		return $this;
	}

	/**
	 * Skips a number of results
	 *
	 * $param int $num
	 * @return \P3\Mongo\Cursor
	 */
	public function skip($num) {
		$this->cursor->skip($num);
		return $this;
	}

	/**
	 * Sets whether this cursor will timeout
	 *
	 * @param  bool $liveForever
	 * @return \P3\Mongo\Cursor
	 */
	public function immortal($liveForever=true) {
		$this->cursor->immortal($liveForever);
		return $this;
	}

	/**
	 * Gives the database a hint about the query
	 *
	 * @param array $key_pattern
	 * @return \P3\Mongo\Cursor
	 */
	public function hint(array $key_pattern) {
		$this->cursor->hint($key_pattern);
		return $this;
	}

	/**
	 * Gets the query, fields, limit, and skip for this cursor
	 * @return array
	 */
	public function info() {
		return $this->cursor->info();
	}

	/**
	 * Limits the number of results returned
	 *
	 * $param int $num
	 * @return \P3\Mongo\Cursor
	 */
	public function limit($num) {
		$this->cursor->limit($num);
		return $this;
	}

	/**
	 * Sets the fields for a query
	 *
	 * $param array $f
	 * @return \P3\Mongo\Cursor
	 */
	public function fields(array $f) {
		$this->cursor->fields($f);
		return $this;
	}

	/**
	 * Return an explanation of the query, often useful for optimization and debugging
	 * @return array
	 */
	public function explain() {
	//	try {
			$found = $this->cursor->explain();
		
	//		$this->errorCount = 0;
			return $found;
			
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->explain();
	//	}
	}

	/**
	 * Execute the query.
	 */
	public function doQuery() {
	//	try {
			$this->cursor->doQuery();
	//		$this->errorCount = 0;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}
	//
	//		sleep(1);
	//		$this->errorCount++;
	//
	//		return $this->doQuery();
	//	}
	}

	/**
	 * Checks if there are documents that have not been sent yet from the database for this cursor
	 * @return bool  Returns if there are more results that have not been sent to the client, yet.
	 */
	public function dead() {
		return $this->cursor->dead();
	}

	/**
	 * Checks if there are any more elements in this cursor
	 * @return bool
	 */
	public function hasNext() {
	
		//try {
			$found = $this->cursor->hasNext();
		
		//	$this->errorCount = 0;
			return $found;
			
		//} catch(\MongoCursorException $e) {
		//	if($this->errorCount >= self::MAX_ERROR_TRY) {
		//		throw $e;
		//	}

		//	sleep(1);
		//	$this->errorCount++;
		//
		//	return $this->hasNext();
		//}
	}

	/**
	 * Return the next object to which this cursor points, and advance the cursor
	 * @return mixed
	 */
	public function getNext() {
		
	//	try {
			$found = $this->cursor->getNext();
		
	//		$this->errorCount = 0;
			return $found;
	//	
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->getNext();
	//	}
	}

	/**
	 * Returns the cursor to the beginning of the result set
	 */
	function rewind() {
		$this->cursor->rewind();
	}

	/**
	 * Clears the cursor
	 */
	public function reset() {
		$this->cursor->reset();
	}

	/**
	 * Returns the current result's _id
	 * return string
	 */
	function key() {
		return $this->cursor->key();
	}

	/**
	 * Advances the cursor to the next result
	 */
	function next() {
	//	try {
			$this->cursor->next();
	//		$this->errorCount = 0;

	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->next();
	//	}
	}

	/**
	 * Checks if the cursor is reading a valid result.
	 * 
	 * @return bool
	 */
	function valid() {
		return $this->cursor->valid();
	}

	/**
	 * Adds a top-level key/value pair to a query
	 *
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return \P3\Mongo\Cursor
	 */
	public function addOption($key, $value) {
		$this->cursor->addOption($key, $value);
		return $this;
	}

	/**
	 * Counts the number of results for this query
	 *
	 * @param  bool $all Send cursor limit and skip information to the count function, if applicable.
	 * @return int
	 */
	public function count($all=false) {
	//	try {
			$found = $this->cursor->count($all);
			
	//		$this->errorCount = 0;
			return $found;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->count($all);
	//	}
	}

	/**
	 * @return mixed
	 */
	function current() {
		$current = $this->cursor->current();
		$object  = \P3\Mongo\Hydrate\Object::perform($current, $this->objectName, $this->db, $this->collectionName);

		\P3\Mongo\Listener\Manager::perform($object, 'afterRetrieve', $this->db);

		return $object;
	}

}
