<?php
namespace P3\Mongo\Iterator;

/**
 * @author Samuel Carlier
 */
class Reference implements \Iterator, \Countable, \ArrayAccess, \SeekableIterator {

	/**
	 * @var array
	 */
	private $data = array();

	/**
	 * @var bool
	 */
	private $valid = true;

	/**
	 * @var \P3\Mongo\Manager
	 */
	private $db;

	/**
	 * @param array $data
	 * @param \P3\Mongo\Manager $db
	 */
	public function __construct(array $data, $db) {
		$this->db   =  $db;
		$this->data = $data;
	}

	/**
	 * @return int
	 */
	public function count() {
		return count($this->data);
	}

	/**
	 * 
	 */
	function rewind() {
		reset($this->data);
	}

	/**
	 * @return mixed
	 */
	function current() {
		return $this->getRef($this->key());
	}

	/**
	 * return string
	 */
	function key() {
		return key($this->data);
	}

	/**
	 *
	 */
	function next() {
		$this->valid = next($this->data);
	}

	/**
	 *
	 * @return bool
	 */
	function valid() {
		return $this->valid === false ? false : true;
	}

	/**
	 * @param string $offset
	 * @param mixed  $value
	 */
	public function offsetSet($offset, $value) {
		$this->data[$offset] = $value;
	}

	/**
	 * @param  string $offset
	 * @return bool
	 */
	public function offsetExists($offset) {
		return isset($this->data[$offset]);
	}

	/**
	 * @param string $offset
	 */
	public function offsetUnset($offset) {
		unset($this->data[$offset]);
	}

	/**
	 * @param  string $offset
	 * @return mixed
	 */
	public function offsetGet($offset) {
		return isset($this->data[$offset]) ? $this->getRef($offset) : null;
	}

	/**
	 * @param int $position
	 */
	public function seek($position) {
		if($position > $this->count() - 1) {
			throw Exception::invalidSeek($position);
		}

		$this->rewind();
		$counter = 0;

		while(true) {
			if($position == $counter) {
				break;
			}

			$this->next();
			$counter++;
		}
	}

	/**
	 * @return object
	 */
	private function getRef($key) {
		$data = $this->data[$key];

		if(!is_array($data) || !isset($data['_primary']) && !isset($data['_classname'])) {
			return $data;
		}

		$this->data[$key] = $this->db->getDBRef($data);
		return $this->data[$key];
	}
}