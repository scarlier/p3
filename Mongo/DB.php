<?php
namespace P3\Mongo;

/**
 * @author Samuel Carlier
 */
class DB {

	/**
	 *
	 */
//	const MAX_ERROR_TRY = 4;

	/**
	 * @var int
	 */
//	private $errorCount = 0;

	/**
	 * @var \MongoDB
	 */
	private $db;

	/**
	 * @param \MongoDB $db
	 * @param string   $name
	 */
	public function __construct(\MongoDB $db) {
		$this->db = $db;
	}

	/**
	 * @param  array $command
	 * @return array
	 */
	public function command(array $command) {
	//	try {
			$returnset = $this->db->command($command);
	//		$this->errorCount = 0;
	
			return $returnset;

	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->command($command);
	//	}
	}

	/**
	 * The name of this database
	 * 
	 * @return string
	 */
	public function __toString() {
		return (string) $this->db;
	}

	/**
	 * @param string $username
	 * @param string $password
	 *
	 * @return true if success throw exception otherwise
	 */
	public function authenticate($username, $password) {
		$auth = $this->db->authenticate($username, $password);
		if($auth['ok']) {
			return true;
		}

		throw Exception::authFailed($auth['errmsg'], __METHOD__);
	}

	/**
	 * Repairs and compacts this database
	 *
	 * @param bool $preserve_cloned_files If cloned files should be kept if the repair fails.
	 * @param bool $backup_original_files If original files should be backed up.
	 *
	 * @return TRUE if success Exception otherwise
	 */
	public function repair($preserve_cloned_files=false,  $backup_original_files=false) {
		$repair = $this->db->repair($preserve_cloned_files, $backup_original_files);
		if($repair['ok']) {
			return true;
		}

		throw Exception::repairFailed((string) $this->db, $repair);
	}

	/**
	 * Drops this database
	 *
	 * @return TRUE if success Exception otherwise
	 */
	public function drop() {
		$drop = $this->db->drop();
		if($drop['ok']) {
			return true;
		}

		throw Exception::dropDBFailed((string) $this->db, $drop);
	}

	/**
	 * Sets this database's profiling level
	 *
	 * @param int $lvl
	 * @return int Returns the previous profiling level.
	 */
	public function setProfilingLevel($lvl) {
		return $this->db->setProfilingLevel($lvl);
	}

	/**
	 * Gets this database's profiling level
	 *
	 * @return int
	 */
	public function getProfilingLevel() {
		return $this->db->getProfilingLevel();
	}

	/**
	 * Returns the error, if there was one. 
	 *
	 * return array
	 */
	public function lastError() {
		return $this->db->lastError();
	}

	/**
	 * Checks for the last error thrown during a database operation
	 * Returns the error and the number of operations ago it occurred.
	 *
	 * @return array
	 */
	public function prevError() {
		return $this->db->prevError();
	}

	/**
	 * Clears any flagged errors on the database
	 * 
	 * @return array
	 */
	public function resetError() {
		return $this->db->resetError();
	}

	/**
	 * Get a list of collections in this database
	 *
	 * @return array
	 */
	public function listCollections() {
		return $this->db->listCollections();
	}

	/**
	 * @param  array $a
	 * @return mixed
	 */
	public function getDBRef(array $a) {
		$values = $this->getCollection($a['_classname'])->findOne(array('_id' => new \MongoId($a['_primary'])));
		return $values;
	}

	/**
	 * @param string $name
	 * @return \P3\Mongo\Collection
	 */
	public function getCollection($name) {
		$name = ltrim($name, '\\');
		$collectionName = '';
		
		if(($pos = strpos($name, ':')) !== false) {
			$collectionName = substr($name, $pos + 1);
			$name = substr($name, 0, $pos);
		}
		
		if(empty($collectionName)) {
			$reflection		= new \P3\Mongo\Reflection\ReflectionClass($name);
			$collectionName = $reflection->getCollectionName();
		}

		return new Collection($this, $this->db->{$collectionName}, $name);
	}

	/**
	 * @param string $name
	 * @return \P3\Mongo\Collection
	 */
	public function __get($name) {
		return $this->getCollection($name);
	}

	/**
	 * @param object $object
	 * @param array  $options
	 */
	public function remove($object, array $options=array()) {
		$options = $this->mergeOptions($options);
	
	//	try {
			if(!is_object($object)) {
				throw Exception::invalidObject(__METHOD__);
			}

			$reflection = new Reflection\ReflectionClass($object);
			$primary = $reflection->getPrimary();

			$criteria = array('_id' => new \MongoId($primary->getValue($object)));
			$this->db->{$reflection->getCollectionName()}->remove($criteria, $options);
		//	$this->{get_class($object)}->remove($criteria, $options);
		
	//		$this->errorCount = 0;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->remove($object, $options);
	//	}
	}

	/**
	 * @param object $object
	 * @param int	 $index
	 * @param array  $options
	 */
	public function removeRef($object, $index, array $options=array()) {
		$this->remove($object[$index], $options);
		unset($object[$index]);
	}

	/**
	 * @param mixed object
	 * @param array $options
	 *
	 * @return object after a successful save
	 *		   null if there isn't a valid entity to save
	 */
	public function save($object, array $options=array()) {
		$options = $this->mergeOptions($options);
	
	//	try {
			if(!is_object($object)) {
				throw \P3\Mongo\Exception::invalidObject(__METHOD__);
			}

			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_start_time = microtime(true);
			}

			$reflection = new Reflection\ReflectionClass($object);
			$collection = $this->db->{$reflection->getCollectionName()};

			//generate array for mongodb
			$vars = $this->objectVars($object);
			if($vars === false) {
				return null;
			}

			//save data to mongo
			$collection->save($vars['columnData'], $options);

			//update primary key with MongoId
			$reflection->getPrimary()->setValue($object, (string) $vars['columnData']['_id']);

			//update references
	//		if(!empty($vars['refs'])) {
	//			$primaryId = $reflection->getPrimary()->getValue($object);
	//			$this->saveRef($vars['refs'], $collection, $primaryId);
	//		}

			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_total_time = microtime(true) - $logger_start_time;
				\P3\Mongo\Logger\Manager::profileSave($logger_total_time, array('object' => $object, 'vars' => $vars['columnData']));
			}

			\P3\Mongo\Listener\Manager::perform($object, 'afterSave', $this);

	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->save($object, $options);
	//	}
		
		
		//outside try -> otherwise if findone get's a exception its another save :(
		if(!empty($vars['refs'])) { //retrieve document for proper reference iterators
			$object = $this->getCollection($reflection->getName())->findOne(array('_id' => new \MongoId($primaryId)));
		}

		//return current object
	//	$this->errorCount = 0; //reset

		return $object;
	}

	/**
	 * @var  object $object
	 * @param bool  $embed
	 */
	private function objectVars($object, $embed=false) {
		$reflection = new Reflection\ReflectionClass($object);
		if(!$reflection->isEntity()) {
			return false;
		}

		\P3\Mongo\Listener\Manager::perform($object, 'preSave', $this);

		$returnSet  = array();
		$values		= array();

		/* @var $property \P3\Mongo\Reflection\Property */
		foreach($reflection->getProperties() as $property) {
			if(!$property->hasType()) {
				continue;
			}

			$name = $property->getName();
			$values[$name] = $property->getValue($object);

			//reference collections
			if($property->isReference() && !empty($values[$name]) && count($values[$name]) > 0) {
				$values[$name] = $this->propertyRef($object, $property);
				//$returnSet['refs'] = $this->propertyRef($object, $property);
			}

			elseif( ($property->isReference() || $property->isEmbed()) && !empty($values[$name]) && count($values[$name]) === 0) {
				$values[$name] = array();
			}

			//embedded documents
			elseif($property->isEmbed() && !empty($values[$name]) && count($values[$name]) > 0) {
				$embedded_documents = $values[$name];
				$is_multi_embedded  = is_array($embedded_documents) ? true : false;
				$values[$name]	   = array();

				//it is not a array make it for processing
				if(!$is_multi_embedded) {
					$embedded_documents = array($embedded_documents);
				}

				foreach($embedded_documents as $embedded_document) {
					$doc = $this->objectVars($embedded_document, true);
					if($doc !== false) {
						$values[$name][][\get_class($embedded_document)] = $doc['columnData'];
					}
				}

				//remove the array.. processing is done
				if(!$is_multi_embedded) {
					$values[$name] = $values[$name][0];
				}

				$embedded_documents = null;
			}

			//PHP DateTime to MongoDate
			elseif($property->isDate()) {
				if(empty($values[$name])) {
					continue;
				}

				if(!$values[$name] instanceof \DateTime) {
					throw \P3\Mongo\Exception::dateFieldTypeError($reflection->getName(), $name);
				}

				$values[$name] = new \MongoDate($values[$name]->getTimestamp());
			}
		}

		if(!$embed) {
			$primary = $reflection->getPrimary();
			if(empty($primary)) {
				throw \P3\Mongo\Exception::missingPrimaryProperty($reflection->getName());
			}

			$primary = $primary->getValue($object);
			if($primary) {
				$values['_id'] = new \MongoId($primary);
			}
		}

		$returnSet['columnData'] = $values;
		return $returnSet;
	}


	/**
	 * @param object $object
	 * @param \P3\Mongo\Reflection\Property $property
	 */
	private function propertyRef($object, Reflection\Property $property) {
		$refData = $property->getValue($object);

		if(!is_array($refData) && !$refData instanceof \P3\Mongo\Iterator\Reference) {
			$refData = array($refData);
		}

		$refs = array();
		foreach($refData as $rd) {

			if(is_array($rd) && array_key_exists('_primary', $rd) && array_key_exists('_classname', $rd)) {
				$refs[] = $rd;
			} 

			elseif(is_object($rd)) {
				$reflection = new \P3\Mongo\Reflection\ReflectionClass($rd);
				$classname  = $reflection->getName();

				if(!$reflection->getPrimary() || !$reflection->getPrimary()->getValue($rd)) {
					throw Exception::referenceMissingPrimary();
				}

				$refs[]	= $this->getCollection($classname)->createDBRef($classname, $reflection->getPrimary()->getValue($rd));
			}

			else {
				throw Exception::invalidReference();
			}
		}

		return $refs;
	}

//	private function propertyRef($object, Reflection\Property $property) {
//		$refData = $property->getValue($object);
//		$one2one = false;
//
//		if(!is_array($refData) && !$refData instanceof \P3\Mongo\Iterator\Reference) {
//			$refData = array($refData);
//		}
//
//		$refs = array();
//		foreach($refData as $rd) {
//
//			print_r($rd);
//
//			$saved = $this->save($rd);
//			$refs[] = array('saved' => $saved, 'name' => $property->getName());
//		}
//
//		return $refs;
//	}

	/**
	 * @param array $refs
	 * @param \MongoCollection $collection
	 * @param string $toRef
	 *
	 * @return array
	 */
//	private function saveRef(array $refs, \MongoCollection $collection, $toRef) {
//
//		foreach($refs as $ref) {
//			$reflection = new Reflection\ReflectionClass($ref['saved']);
//
//			$ref_collection = $this->getCollection($reflection->getName());
//			$id = $reflection->getPrimary()->getValue($ref['saved']);
//
//			$item	   = $ref_collection->findOne(array('_id' => new \MongoId($id)));
//			$refToItem = $ref_collection->createDBRef($reflection->getName(), $id);
//
//			$refData   = array('$push' => array($ref['name'] => $refToItem));
//
//			$collection->update(array('_id' => new \MongoId($toRef)), $refData);
//		}
//
//		return $item;
//	}

	/**
	 * merge new options with default
	 * 
	 * @param  array $newOptions
	 * @return array
	 */
	private function mergeOptions(array $newOptions) {
		$options = Manager::getGlobalOptions();
		
		foreach($newOptions as $key => $value) {
			$options[$key] = $value;
		}
		
		return $options;
	}
}
