<?php
namespace P3\Mongo\Listener;

/**
 * @author Samuel Carlier
 */
class Args {

	/**
	 * @var mixed
	 */
	private $object;

	/**
	 * @var \P3\Mongo\DB
	 */
	private $db;

	/**
	 * @param mixed $object
	 */
	public function __construct($object, \P3\Mongo\DB $db) {
		$this->object = $object;
		$this->db = $db;
	}

	/**
	 * @return mixed
	 */
	public function getObject() {
		return $this->object;
	}

	/**
	 * @return \P3\Mongo\DB
	 */
	public function getDB() {
		return $this->db;
	}

	/**
	 * @return \P3\Mongo\Reflection\ReflectionClass
	 */
	public function getReflection() {
		$reflection = new \P3\Mongo\Reflection\ReflectionClass($this->object);
		return $reflection;
	}
}