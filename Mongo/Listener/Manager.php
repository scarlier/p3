<?php
namespace P3\Mongo\Listener;

/**
 * @author Samuel Carlier
 */
class Manager {

	/**
	 * @var array
	 */
	private static $listeners = array();

	/**
	 * static class
	 */
	private function __construct() {}

	/**
	 * @param mixed  $object
	 * @param string $type
	 * @param \P3\Mongo\DB $db
	 */
	public static function perform($object, $type, $db) {
		$listeners = self::getType($type);
		$args	   = new Args($object, $db);

		foreach($listeners as $listener) {
			$listener['object']->$type($args);
		}
	}

	/**
	 * @param string $methodName
	 */
	private static function getType($methodName) {
		$found = array();

		foreach(self::$listeners as $listener) {
			if(in_array($methodName, $listener['types'])) {
				$found[] = $listener;
			}
		}

		return $found;
	}

	/**
	 * @param $listener
	 */
	public static function addListener($listener) {
		$types = self::listenerTypes($listener);

		self::$listeners[] = array( 'object' => $listener,
									'types'	 => $types,
									'name'	 => get_class($listener),
									'hash'	 => \spl_object_hash($listener)
		);
	}

	/**
	 * @param mixed $name
	 */
	public static function removeListener($name) {
		$hash = null;
		if(\is_object($name)) {
			$name = \get_class($name);
			$hash = \spl_object_hash($name);
		}

		foreach(self::$listeners as $key => $listener) {
			if($listener['name'] != $name) {
				continue;
			}

			if($hash && $hash !== $listener['hash']) {
				continue;
			}

			unset(self::$listeners[$key]);
		}
	}

	/**
	 * Remove all listeners
	 */
	public function clear() {
		self::$listeners = array();
	}

	/**
	 * @param object $listener
	 * @return array
	 */
	private static function listenerTypes(\P3\Mongo\Listener\Crud $listener) {
		$reflection = new \ReflectionObject($listener);
		$methods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);


		$supported = array();
		foreach($methods as $method) {
			$method = $method->getName();
			if(!self::isSupportedMethod($method)) {
				continue;
			}

			$supported[] = $method;
		}

		return $supported;
	}

	/**
	 * @param  string $methodName
	 * @return bool
	 */
	private static function isSupportedMethod($methodName) {
		$supported = array('preSave', 'afterSave', 'afterRetrieve');

		if(\in_array($methodName, $supported)) {
			return true;
		}

		return false;
	}
}
