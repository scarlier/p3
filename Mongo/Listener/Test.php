<?php
namespace P3\Mongo\Listener;


/*
 * 
 */
class Test {

	/**
	 * @param \P3\Mongo\Listener\Args $args
	 */
	public function preSave(\P3\Mongo\Listener\Args $args) {
		echo __METHOD__ . '<br>' . "\n";
	}

	/**
	 * @param \P3\Mongo\Listener\Args $args
	 */
	public function afterRetrieve(\P3\Mongo\Listener\Args $args) {
		echo __METHOD__ . '<br>' . "\n";
	}

	/**
	 * @param \P3\Mongo\Listener\Args $args
	 */
	public function afterSave(\P3\Mongo\Listener\Args $args) {
		echo __METHOD__ . '<br>' . "\n";
	}
}

