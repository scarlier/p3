<?php
namespace P3\Mongo\Logger;

/**
 * @author Samuel Carlier
 */
interface Log {

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileSave($ms, array $data);

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileRetrieve($ms, array $data);
	
}