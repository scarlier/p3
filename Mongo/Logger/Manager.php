<?php
namespace P3\Mongo\Logger;

/**
 * @author Samuel Carlier
 */
class Manager {

	/**
	 * @var array
	 */
	private static $loggers = array();

	/**
	 * static class
	 */
	private function __construct() {}

	/**
	 * @param string $method
	 * @param int	 $ms
	 * @param mixed	 $arg1
	 */
	private static function perform($method, $ms, $arg1) {
		foreach(self::$loggers as $logger) {
			$logger::$method($ms, $arg1);
		}
	}

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileSave($ms, array $data) {
		self::perform('profileSave', $ms, $data);
	}

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileRetrieve($ms, array $data) {
		self::perform('profileRetrieve', $ms, $data);
	}

	/**
	 * @return bool
	 */
	public static function hasLoggers() {
		return !empty(self::$loggers);
	}

	/**
	 * @param \P3\Mongo\Logger\Log $logger
	 */
	public static function addLogger(\P3\Mongo\Logger\Log $logger) {
		self::$loggers[] = $logger;
	}

	/**
	 * @param mixed $name
	 */
	public static function removeLogger($name) {

		$hash = null;
		if(\is_object($name)) {
			$name = \get_class($name);
			$hash = \spl_object_hash($name);
		}

		foreach(self::$loggers as $key => $logger) {
			if(get_class($logger) != $name) {
				continue;
			}

			if($hash && $hash !== \spl_object_hash($logger)) {
				continue;
			}

			unset(self::$loggers[$key]);
		}
	}

	/**
	 * Remove all listeners
	 */
	public function clear() {
		self::$loggers = array();
	}
}
