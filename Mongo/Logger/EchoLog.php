<?php
namespace P3\Mongo\Logger;

/**
 * @author Samuel Carlier
 */
class EchoLog implements Log {

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileSave($ms, array $data) {
		echo 'save --  (' . round($ms, 6) . ' ms) -- ' . get_class($data['object']) . "\n";
	}

	/**
	 * @param int	$ms
	 * @param array $data
	 */
	public static function profileRetrieve($ms, array $data) {
		echo 'retrieve -- ' . round($ms, 6) . ' ms -- ' . json_encode($data['query']) . "\n";
	}
}

