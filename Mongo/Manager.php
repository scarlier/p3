<?php
namespace P3\Mongo;

/**
 * @author Samuel Carlier
 */
class Manager {

	/**
	 * @var \Mongo
	 */
	private $mongo;

	/**
	 * @var bool
	 */
	private static $cache = false;
	
	/**
	 * @var bool
	 */
	private static $globalOptions = array();

	/**
	 * @param string $server
	 * @param array  $options
	 */
	public function __construct($server=null, array $options=array()) {
		if($server !== null && !is_string($server)) {
			throw Exception::notString($server, __METHOD__);
		}

		$this->mongo = !$server ? new \Mongo : new \Mongo($server, $options);
	}

	/**
	 * @param bool $cache
	 */
	public static function isCacheEnabled($cache=null) {
		if(!is_null($cache)) {
			self::$cache = $cache;
		}
		
		return self::$cache;
	}
	
	/**
	 * @param $options
	 */
	public static function setGlobalOptions(array $options) {
		self::$globalOptions = $options;
	}
	
	/**
	 * @return array
	 */
	public static function getGlobalOptions() {
		return self::$globalOptions;
	}

	/**
	 * String representation of this connection
	 * Returns hostname and port for this connection. 
	 * 
	 * @return string
	 */
	public function __toString() {
		return (string) $this->mongo;
	}

	/**
	 * Closes this database connection
	 */
	public function close() {
		$this->mongo->close();
	}

	/**
	 * Connects with a database server
	 * Throws \MongoConnectionException if it fails to connect to the databases.
	 *
	 * @return bool If the connection was successful.
	 */
	public function connectUtil() {
		$this->mongo->connectUtil();
	}

	/**
	 * Connects to a database server
	 */
	public function connect() {
		$this->mongo->connect();
	}

	/**
	 * Gets a database
	 * 
	 * @param string $db
	 * @return \P3\Mongo\DB
	 */
	public function selectDB($db) {
		return new DB($this->mongo->selectDB($db));
	}

	/**
	 * gets a database
	 *
	 * @paramf string $db
	 * @return \P3\Mongo\DB
	 */
	public function __get($db) {
		return $this->selectDB($db);
	}

	/**
	 * Lists all of the databases available.
	 * 
	 * @return array
	 */
	public function listDBs() {
		return $this->mongo->listDBs();
	}
}
