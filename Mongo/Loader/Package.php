<?php
namespace P3\Mongo\Loader;

/**
 * @author Samuel Carlier
 */
class Package {

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $separator;

	/**
	 * @param string $path
	 * @param string $separator
	 */
	public function __construct($path, $separator='\\') {
		$this->path = $path;
		$this->separator = $separator;

		$this->register();
	}

	/**
	 *
	 */
	public function register() {
		spl_autoload_register(array($this, 'load'));
	}

	/**
	 *
	 */
	public function unregister() {
		spl_autoload_unregister(array($this, 'load'));
	}

	/**
	 * @param string $className
	 */
	public function load($className) {
        $file = str_replace($this->separator, DIRECTORY_SEPARATOR, $className);
        $file = substr($file, strpos($file, DIRECTORY_SEPARATOR) + 1);

        if(!file_exists($this->path . $file . '.php')) {
        	return false;
        }

        require $this->path . $file . '.php';

        return true;
	}
}