<?php
namespace P3\Mongo;

class Collection implements \Countable {

	/**
	 *
	 */
//	const MAX_ERROR_TRY = 4;

	/**
	 * @var int
	 */
//	private $errorCount = 0;

	/**
	 * @var MongoCollection
	 */
	private $collection;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var \P3\Mongo\DB
	 */
	private $db;

	/**
	 * @param \P3\Mongo\DB $db
	 * @param \MongoCollection  $collection
	 * @param string $name
	 */
	public function __construct(\P3\Mongo\DB $db, \MongoCollection $collection, $name) {
		$this->db	= $db;
		$this->name = $name;

		$this->collection = $collection;
	}

	/**
	 * merge new options with default
	 * 
	 * @param  array $newOptions
	 * @return array
	 */
	private function mergeOptions(array $newOptions) {
		$options = Manager::getGlobalOptions();
		
		foreach($newOptions as $key => $value) {
			$options[$key] = $value;
		}
		
		return $options;
	}

	/**
	 *
	 * @param string $criteria Description of the objects to update.
	 * @param array $newobj  The object with which to update the matching records.
	 * @param array $options
	 *
	 * @return bool
	 */
	public function update($criteria, array $newobj, array $options = array()) {
		$options = $this->mergeOptions($options);
		
	//	try {
			$returnset = $this->collection->update($criteria, $newobj, $options);
	//		$this->errorCount = 0;
			
			return $returnset;
			
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->update($criteria, $newobj, $options);
	//	}
	}

	/**
	 * @param  string $classname
	 * @param  string $primary
	 * @return array
	 */
	public function createDBRef($classname, $primary) {
		//$ref = $this->collection->createDBRef($a);

		$ref = array();
		$ref['_primary']   = $primary;
		$ref['_classname'] = $classname;

		return $ref;
	}

	/**
	 * Validates this collection
	 *
	 * @param  bool $scan_data Only validate indices, not the base collection.
	 * @return array  Returns the database's evaluation of this object.
	 */
	public function validate($scan_data=false) {
		return $this->collection->validate($scan_data);
	}

	/**
	 * String representation of this collection
	 * @return string Returns the full name of this collection.
	 */
	public function __toString() {
		return (string) $this->collection;
	}

	/**
	 * Returns this collection's name
	 * @return string
	 */
	public function getName() {
		return $this->collection->getName();
	}

	/**
	 * Returns an array of index names for this collection
	 * @return array
	 */
	public function getIndexInfo() {
		return $this->collection->getIndexInfo();
	}

	/**
	 * Drops this collection and deletes its indices.
	 * @return TRUE if success Exception otherwise
	 */
	public function drop() {
		$drop = $this->collection->drop();
		if($drop['ok']) {
			return true;
		}

		throw Exception::dropCollectionFailed((string) $this->collection, $drop);
	}

	/**
	 * Delete all indices for this collection
	 * @return TRUE if success Exception otherwise
	 */
	public function deleteIndexes() {
		$delIndex = $this->collection->deleteIndexes();
		if($delIndex['ok']) {
			return true;
		}

		throw Exception::deleteIndexesFailed((string) $this->collection, $drop);
	}

	/**
	 * Deletes an index from this collection
	 *
	 * @param string|array $keys
	 * @return TRUE if success Exception otherwise
	 */ 
	public function deleteIndex($keys) {
		$delIndex = $this->collection->deleteIndex($keys);
		if($delIndex['ok']) {
			return true;
		}

		throw Exception::deleteIndexesFailed((string) $this->collection, $keys);
	}

	/**
	 * Creates an index on the given field(s), or does nothing if the index already exists
	 *
	 * @param array $keys
	 * @param array $options
	 *
	 * @return bool
	 */
	public function ensureIndex(array $keys, array $options=array()) {
		$options = $this->mergeOptions($options);
		
		
	//	try {
			$returnset = $this->collection->ensureIndex($keys, $options);
	//		$this->errorCount = 0;
			
			return $returnset;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->ensureIndex($keys, $options);
	//	}
	}

	/**
	 * Counts the number of documents in this collection
	 *
	 * @param array $query Array or object with fields to match.
	 * @return int
	 */
	public function count(array $query = array()) {
		return $this->collection->count($query);
	}

	/**
	 * @param mixed $criteria
	 * @param array $options
	 */
	public function remove($criteria, array $options=array()) {
		$options = $this->mergeOptions($options);

	//	try {
			if(is_object($criteria)) {
				$criteria = array('_id' => new \MongoId(Helper::getPrimaryValue($criteria)));
			}

			$this->collection->remove($criteria, $options);
	//		$this->errorCount = 0;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->remove($criteria, $options);
	//	}
	}

	/**
	 * @param string $value
	 * @param array  $fields
	 */
	public function findID($value, array $fields = array()) {
	//	try {
			$found = $this->findOne(array('_id' => new \MongoId($value)), $fields);
			
	//		$this->errorCount = 0;
			return $found;

	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->findID($value, $fields);
	//	}
	}

	/**
	 * @param array $query
	 * @param array $update
	 * @param array $sort
	 * @param array $options 
	 *
	 * @return mixed
	 *		   false if not found
	 */
	public function findAndModify(array $query, array $update, array $sort = array(), array $options = array()) {
		if(\P3\Mongo\Logger\Manager::hasLoggers()) {
			$logger_start_time = microtime(true);
		}

		$options = $this->mergeOptions($options);
		$values = $this->db->command(array(
			'findandmodify' => $this->collection->getName(), 
			'query' => $query, 
			'sort' => $sort,
			'new' => true,
			'update' => $update
		), $options); 

		if(\P3\Mongo\Logger\Manager::hasLoggers()) {
			$logger_total_time = microtime(true) - $logger_start_time;
			\P3\Mongo\Logger\Manager::profileRetrieve($logger_total_time, array('query' => $query, 'update' => $update, 'sort' => $sort));
		}

		if(!$values || empty($values['ok'])) {
			return false;
		}
		
		$object = \P3\Mongo\Hydrate\Object::perform($values['value'], $this->name, $this->db, $this->getName());
		\P3\Mongo\Listener\Manager::perform($object, 'afterRetrieve', $this->db);

		return $object;
	}

	/**
	 * @param mixed $keys
	 * @param array $initial
	 * @param \MongoCode $reduce
	 * 
	 * @return array
	 *		   false on error
	 */
	public function group($keys, $initial, $reduce, $options=array()) {
		$data = $this->collection->group($keys, $initial, $reduce, $options);
		if(!array_key_exists('retval', $data)) {
			return false;
		}

		return $data['retval'];
	}


	/**
	 * Querys this collection and returns a entity
	 *
	 * @param  array $query
	 * @param  array $fields
	 *
	 * @return mixed
	 */
	public function findOne(array $query, array $fields = array()) {
		$queryRaw = $query;
	
	//	try {
			foreach($query as $key => $value) {
				if(!is_object($value) || $value instanceof \MongoId) {
					continue;
				}

				$reflection = new \P3\Mongo\Reflection\ReflectionClass($value);
				$id = $reflection->getPrimary()->getValue($value);

				$query[$key . '._primary'] = $id;
				unset($query[$key]);
			}

			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_start_time = microtime(true);
			}

			$values = $this->collection->findOne($query, $fields);

			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_total_time = microtime(true) - $logger_start_time;
				\P3\Mongo\Logger\Manager::profileRetrieve($logger_total_time, array('query' => $query, 'fields' => $fields));
			}

			if($values === null) {
				return null;
			}

			$object = \P3\Mongo\Hydrate\Object::perform($values, $this->name, $this->db, $this->getName());
			\P3\Mongo\Listener\Manager::perform($object, 'afterRetrieve', $this->db);

	//		$this->errorCount = 0;
			return $object;
		
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->findOne($queryRaw, $fields);
	//	}
	}

	/**
	 * Querys this collection
	 *
	 * @param  array $query
	 * @param  array $fields
	 *
	 * @return \P3\Mongo\Cursor
	 */
	public function find(array $query = array(), array $fields = array()) {
	//	try {
			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_start_time = microtime(true);
			}

			$cursor = new Cursor($this->collection->find($query, $fields), $this->db, $this->name, $this->getName());

			if(\P3\Mongo\Logger\Manager::hasLoggers()) {
				$logger_total_time = microtime(true) - $logger_start_time;
				\P3\Mongo\Logger\Manager::profileRetrieve($logger_total_time, array('query' => $query, 'fields' => $fields));
			}

	//		$this->errorCount = 0;
			return $cursor;
			
	//	} catch(\MongoCursorException $e) {
	//		if($this->errorCount >= self::MAX_ERROR_TRY) {
	//			throw $e;
	//		}

	//		sleep(1);
	//		$this->errorCount++;

	//		return $this->find($query, $fields);
	//	}
	}
}
