<?php
namespace P3\File;

/**
 * @author Samuel
 */
class Manager {

	/**
	 * @var array
	 */
	private static $protocols = array();

	/**
	 * @param  string $link
	 * @return \P3\File\Adapter\AdapterInterface
	 */
	public static function get($link) {
		return self::getInstance($link);
	}

	/**
	 * @param  string $classname
	 * @return bool
	 */
	public static function register($classname) {
		$classname = trim($classname, '\\');
		if(!class_exists($classname)) {
			return false;
		}

		self::$protocols[str_replace('://', '', $classname::PROTOCOL)] = $classname;
		return true;
	}

	/**
	 * @param string $protocol
	 */
	public static function unregister($protocol) {
		unset(self::$protocols[$protocol]);
	}

	/**
	 * @param  string $link
	 * @return \P3\File\Adapter\AdapterInterface
	 */
	private static function getInstance($link) {
		if(!self::$protocols) {
			self::loadDefaultProtocols();
		}

		$protocol = explode('://', $link);
		if(empty($protocol[0]) || !\array_key_exists($protocol[0], self::$protocols)) {
			throw Exception::invalidProtocol($link);
		}

		return new self::$protocols[$protocol[0]]($link);
	}

	/**
	 * 
	 */
	private static function loadDefaultProtocols() {

	}
}

