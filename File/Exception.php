<?php
namespace P3\File;

/**
 * @author Samuel
 */
class Exception extends \Exception {

	/**
	 * @param string $url
	 * @return \P3\File\Exception
	 */
	public static function invalidProtocol($url) {
		return new self('protocol not valid or found "'.$url.'"');
	}
}

