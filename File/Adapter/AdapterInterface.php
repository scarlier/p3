<?php 
namespace P3\File\Adapter;

/**
 * @author Samuel
 */
interface AdapterInterface {

	/**
	 * @param string $filename
	 */
	public function __construct($filename);

	/**
	 * @return string
	 */
	public function getFileSystemPath();

	/**
	 * @return string
	 */
	public function getUrl();

	/**
	 * return protocol and filename
	 *
	 * @return string
	 */
	public function getLink();

	/**
	 * @return bool
	 */
	public function exists();

	/**
	 * @param  string $filePath
	 * @return bool
	 */
	public function saveFile($filePath);

	/**
	 * @param  string $content
	 * @return bool
	 */
	public function saveContent($content);

	/**
	 * @return bool
	 */
	public function remove();

	/**
	 * @return string
	 */
	public function getFileName();

}

