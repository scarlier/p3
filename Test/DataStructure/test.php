<?php
include '../../init.php';

echo '<pre>';

//Connectie
//----------------------------------------------------------------------------
$connection = new \P3\Mongo\Manager('mongodb://localhost');
$database   = $connection->selectDB('p3test');

//Structure
//----------------------------------------------------------------------------
$structure = new \P3\DataStructure\Structure;

$name = new \P3\DataStructure\Item\Text;
$name->setName('name');
$name->setLabel("Naam", "nl")->setLabel("Name", "en");
$structure->addItem($name);

$description = new \P3\DataStructure\Item\Text;
$description->setName('description')->isMultiLine(true);
$name->setLabel("Beschrijving", "nl")->setLabel("Description", "en");
$structure->addItem($description);

$structure->setCollectionName("datastructure-test1");

$structure = $database->save($structure);
print_r($structure);
echo '<hr>';

//Data
//----------------------------------------------------------------------------


$data = new \P3\DataStructure\Data($structure->getId());
$data->addValue($structure->getItemByPosition(0)->getId(), "Nieuws", "nl");
$data->addValue($structure->getItemByPosition(1)->getId(), "Een beschrijving", "nl");

$data->addValue($structure->getItemByPosition(0)->getId(), "News", "en");
$data->addValue($structure->getItemByPosition(1)->getId(), "SomeDescription", "en");

$data = $database->save($data);

print_r($data);