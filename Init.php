<?php
namespace P3;

/**
 * @author 	Samuel Carlier
 */
class Init {

	/**
	 *
	 */
	static public function exec() {
		$current_dir = dirname(__FILE__);

		require $current_dir . '/Autoload/Loader.php';
		require $current_dir . '/Autoload/Package.php';

		\P3\Autoload\Loader::addPackage('P3', $current_dir . '/');
	}
}

Init::exec();