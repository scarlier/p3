<?php
namespace P3\Autoload;

/**
 * @author Samuel Carlier
 */
class Loader {

	/**
	 * @var array
	 */
	private static $packages = array();

	/**
	 * @param string $name
	 * @param string $path
	 */
	public static function addPackage($name, $path) {
		self::$packages[$name] = new Package($name, $path);
		self::$packages[$name]->register();
	}

	/**
	 * @param string $name
	 */
	public static function removePackage($name) {
		self::$packages[$name]->unregister();
		unset(self::$packages[$name]);
	}

	/**
	 * @param  string $name
	 * @return string
	 */
	public static function getPackagePath($name) {
		return self::$packages[$name]->getPath();
	}
}
