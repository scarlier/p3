<?php
namespace P3\Autoload;

/**
 * @author Samuel Carlier
 */
class Package {

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $separator = '\\';

	/**
	 * @param string $name
	 * @param string $path
	 */
	public function __construct($name=null, $path=null) {
		$this->name = $name;
		$this->path = $path;
	}

	/**
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}

	/**
	 *
	 */
	public function register() {
		spl_autoload_register(array($this, 'load'));
	}

	/**
	 *
	 */
	public function unregister() {
		spl_autoload_unregister(array($this, 'load'));
	}

	/**
	 * @param string $className
	 */
	public function load($className) {
		$path = $this->path !== null ? $this->path : '';
        $file = str_replace($this->separator, DIRECTORY_SEPARATOR, $className);
        $file = substr($file, strpos($file, DIRECTORY_SEPARATOR) + 1);

        if(!file_exists($path . $file . '.php')) {
        	return false;
        }
        
        require $path . $file . '.php';

        return true;
	}
}