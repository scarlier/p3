<?php
namespace P3\MongoExt\Tree\Embed;

/**
 * @author Samuel Carlier
 * @entity collection=mongoext_tree
 */
class Node extends Child {

	/**
	 * @primary
	 */
	protected $id;

	/**
	 * 
	 */
	protected static $node = array();

	/**
	 *
	 */
	protected static $nodeHashLookup = array();

	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param string $name
	 * @param \P3\Mongo\DB $db
	 *
	 * @return \P3\MongoExt\Tree\Embed\Node
	 */
	public static function getInstance($name, \P3\Mongo\DB $db) {
		$classname = get_called_class();

		if(!array_key_exists($name, self::$node)) {
			$reflection = new \P3\Mongo\Reflection\ReflectionClass($classname);
			$collection = $reflection->getName();

			$object = $classname::fetchDocument($db, $collection, $name);
			if(empty($object)) {
				$object = new $classname($name);
			}

			self::$node[$name] = $object;
			self::$nodeHashLookup[$object->getHash()] = $name;
		}

		return self::$node[$name];
	}

	/**
	 * getInstance moet nog worden gemaakt zodat het ophalen in DAO kan blijven
	 *
	 * @param \P3\MongoExt\Tree\Embed\Node $node
	 * @param string $name
	 */
	public static function registerNode($node, $name=null) {
		$name = $name ?: md5(uniqid(true));

		self::$node[$name] = $node;
		self::$nodeHashLookup[$node->getHash()] = $name;
	}

	/**
	 * @param \P3\Mongo\DB $db
	 * @param string $collection
	 * @param string $name
	 *
	 * @return \P3\MongoExt\Tree\Embed\Node
	 */
	protected static function fetchDocument($db, $collection, $name) {
		return $db->getCollection($collection)->findOne(array('name' => $name));
	}

	/**
	 * @return self
	 */
	protected static function getInstanceByHash($hash) {
		return self::$node[self::$nodeHashLookup[$hash]];
	}
}


