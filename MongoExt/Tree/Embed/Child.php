<?php
namespace P3\MongoExt\Tree\Embed;

/**
 * @author Samuel Carlier
 * @entity
 */
class Child {

	/**
	 * @date
	 */
	protected $insertDate;

	/**
	 * @date
	 */
	protected $editDate;

	/**
	 * @string
	 */
	protected $editUser;

	/**
	 * @string
	 */
	protected $name;

	/**
	 * @string
	 */
	protected $hash;

	/**
	 * @string
	 */
	protected $parent;

	/**
	 * @array
	 */
	protected $ancestors;

	/**
	 * @embed
	 */
	protected $childs = array();

	/**
	 * @param string $name
	 */
	public function __construct($name=null) {
		$this->name = $name;

		if(!$this->hash) {
			$this->hash = \uniqid(rand(10, 90));
		}

		if(!$this->insertDate) {
			$this->insertDate = new \DateTime;
		}
	}

	/**
	 * @param  string $userId
	 * @return self
	 */
	public function editLog($userId) {
		$this->editUser = $userId;
		$this->editDate = new \DateTime;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEditUser() {
		return $this->editUser;
	}

	/**
	 * @return \DateTime
	 */
	public function getInsertDate() {
		return $this->insertDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getEditDate() {
		return $this->editDate;
	}

	/**
	 * @return string
	 */
	public function getHash() {
		return $this->hash;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param array $ancestors
	 */
	public function setAncestors($ancestors) {
		$this->ancestors = $ancestors;
	}

	/**
	 * @return array
	 */
	public function getAncestors() {
		return $this->ancestors;
	}

	/**
	 * @return string
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * @param string $parent
	 */
	public function setParent($parent) {
		$this->parent = $parent;
	}

	/**
	 * @return string
	 */
	public function getPathAsText() {
		$root = $this->getRootNode();
		$text = '';

		if(is_array($this->ancestors)) {
			foreach($this->ancestors as $an) {
				if($an === $root->getHash()) {
					$name = $root->getName();

				} else {
					$name = $root->getChildByHash($an)->getName();
				}

				$text .= '/' . $name;
			}
		}

		return $text;
	}
	
	/**
	 * @return string
	 */
	public function getPathWithoutRoot() {
		$start = strpos($this->getPathAsText(), $this->getRootNode()->getName());

		if($start !== false) {
			$start = $start + strlen($this->getRootNode()->getName());
			$path = substr($this->getPathAsText(), $start);
			if($path == false) {
				$path = '';
			}

		} else {
			$path = $this->getPathAsText();
		}
		
		$path .= '/' . $this->getName();
		return $path;
	}

	/**
	 * @return \P3\MongoExt\Tree\Embed\Node
	 */
	public function getRootNode() {
		if($this instanceof Node) {
			return $this;
		}

		return Node::getInstanceByHash($this->ancestors[0]);
	}

	/**
	 * @param \P3\MongoExt\Tree\Embed\Child $child
	 * @param string $insertType
	 * @param string $targetHash
	 */
	public function addChild(\P3\MongoExt\Tree\Embed\Child $child, $insertType=null, $targetHash=null) {

		//remove child from prev parent
		$parent = $child->getParent();
		if($parent) {
			$root   = $this->getRootNode();
			$parent = $root->getHash() == $parent ? $root : $root->getChildByHash($parent);

			$parent->removeChild($child->getHash());
		}

		$ancestors   = $this->ancestors;
		$ancestors[] = $this->getHash();

		$child->setAncestors($ancestors);
		$child->setParent($this->getHash());

		//add
		if(($insertType == 'next' || $insertType == 'previous') && !empty($targetHash)) {
			$index = null;

			//zoek de child waar de nieuwe child voor of achter geplaats moet worden
			foreach($this->childs as $k => $c) {
				if($c->getHash() == $targetHash) {
					$index = $k;
					break;
				}
			}

			if($index !== null) {
				$index = $insertType == 'next' ? $index + 1 : (int) $index;

				//deze functies zorgen er voor dat een array in het midden van een bestaande array geplaats kan worden
				array_splice($this->childs, $index, count($this->childs), array_merge(array($child), array_slice($this->childs, $index)));

				return;
			}
		}

		$this->childs[] = $child;
	}

	/**
	 * @return array
	 */
	public function getChildHashRecursive() {
		return $this->recursiveChild($this);
	}

	/**
	 * @param array $child
	 */
	private function recursiveChild($child, array $array=array()) {
		$array[] = $child->getHash();

		if(!$child->hasChilds()) {
			return $array;
		}

		$childs = $child->getChilds();
		foreach($childs as $c) {
			$array = $this->recursiveChild($c, $array);
		}

		return $array;
	}

	/**
	 * @return bool
	 */
	public function hasChilds() {
		return !empty($this->childs);
	}

	/**
	 * @param  mixed $hash
	 * @return bool
	 *		   null if child not found
	 */
	public function removeChild($hash) {
		if(is_object($hash)) {
			$hash = $hash->getHash();
		}

		foreach($this->childs as $key => $child) {
			if($child->getHash() == $hash) {
				unset($this->childs[$key]);
				return true;
			}
		}

		return null;
	}

	/**
	 * @param string $key
	 */
	public function __get($key) {
		foreach($this->childs as $child) {
			if(strtolower($child->getName()) == strtolower($key)) {
				return $child;
			}
		}

		return false;
	}

	/**
	 * @param  string $hash
	 * @return \P3\MongoExt\Tree\Embed\Child 
	 *		   false if not found
	 */
	public function getChildByHash($hash) {

		foreach($this->childs as $child) {
			if($child->getHash() === $hash) {
				return $child;
			}

			if(!$child->hasChilds()) {
				continue;
			}

			$response = $child->getChildByHash($hash);
			if($response !== false) {
				return $response;
			}
		}

		return false;
	}

	/**
	 * @return array
	 */
	public function getChilds() {
		return $this->childs;
	}
}

