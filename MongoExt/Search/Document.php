<?php
namespace P3\MongoExt\Search;

/**
 * @author Samuel Carlier
 * @entity collection=mongo_fulltext_search
 */
class Document {

	/**
	 * @primary
	 */
	private $id;

	/**
	 * @string
	 */
	private $name;

	/**
	 * @string
	 */
	private $category;

	/**
	 * @array
	 */
	private $identifier;

	/**
	 * @string
	 */
	private $language;

	/**
	 * @array
	 */
	private $searchAll = array();
	
	/**
	 * @array
	 */
	private $search = array();

	/**
	 * @date
	 */
	private $dateStart;

	/**
	 * @date
	 */
	private $dateEnd;

	/**
	 * 
	 */
	public function __construct() {
		if(!$this->dateEnd) {
			$time = date("m/d/Y", strtotime('+20 Years'));
			$this->dateEnd = \DateTime::createFromFormat('m/d/Y H:i:s', $time  . ' 23:59:59');
		}

		if(!$this->dateStart) {
			$time = date("m/d/Y", strtotime('-1 Day'));
			$this->dateStart = \DateTime::createFromFormat('m/d/Y H:i:s', $time  . ' 00:00:00');
		}
	}
	
	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param  \DateTime $date
	 * @return self
	 */
	public function setDateStart(\DateTime $date) {
		$this->dateStart = $date;
		return $this;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getDateStart() {
		return $this->dateStart;
	}

	/**
	 * @param  \DateTime $date
	 * @return self
	 */
	public function setDateEnd(\DateTime $date) {
		$this->dateEnd = $date;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getPublishEnd() {
		return $this->dateEnd;
	}
	
	/**
	 * @param  string $name
	 * @return self
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param  string $language
	 * @return string
	 */
	public function setLanguage($language) {
		$this->language = strtolower(trim($language));
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @return self
	 */
	public function clearText() {
		$this->search	 = array();
		$this->searchAll = array();

		return $this;
	}

	/**
	 * @param string $name
	 * @param string $text
	 */
	public function addText($name, $text) {
		//name validate
		$indexKey = md5($name);

		$text = strtolower(strip_tags($text));
		$text = preg_replace('/[^a-z0-9]/', ' ', $text);
		$text = trim(preg_replace('/\s+/', ' ', $text));

		if(empty($text)) {
			return;
		}

		$text = explode(' ', $text);

		$searchParts = array();
		while(list(,$value) = each($text)) {
			$value = trim($value);
			if($value === '') {
				continue;
			}

			$this->search[] = $indexKey . ':' . $value;
			$this->searchAll[] = $value;
		}

		$this->search    = array_values(array_unique($this->search));
		$this->searchAll = array_values(array_unique($this->searchAll));

		return $this;
	}
	
	/**
	 * @param  string $name
	 * @return string
	 */
	public function getText($name) {
		$name = md5($name);

		foreach($this->search as $v) {
			if(strpos($v, $name . ':') !== false) {
				return $v;
			}
		}
		
		return false;
	}

	/**
	 * @param  string $category
	 * @return self
	 */
	public function setCategory($category) {
		$this->category = strtolower(trim($category));
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @param string $string
	 * 
	 * @return array
	 *		   string if string is geven as parameter
	 *		   false if string not found
	 */
	public function getIdentifier($string=null) {
		if(is_null($string)) {
			return $this->identifier;
		}

		if(array_key_exists($string, $this->identifier)) {
			return $this->identifier[$string];
		}

		return false;
	}

	/**
	 * @param  array $identifier
	 * @return self
	 */
	public function setIdentifier(array $identifier) {
		$this->identifier = $identifier;
		return $this;
	}
	
	/**
	 * @param  string $identifier
	 * @return self
	 */
	public function addIdentifier($identifier) {
		$this->identifier[] = $identifier;
		return $this;
	}
	
	/**
	 * @return self
	 */
	public function clearIdentifier() {
		$this->identifier = array();
		return $this;
	}
}