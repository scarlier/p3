<?php
namespace P3\MongoExt\Search;

/**
 * @author Samuel Carlier
 */
class Exception extends \Exception {

	/**
	 * @return self
	 */
	public static function invalidConnection() {
		return new self('connection is not a instance of \P3\Mongo\DB');
	}

	/**
	 * @return self
	 */
	public static function invalidCollection() {
		return new self('collection is not a string');
	}

	/**
	 * @return self
	 */
	public static function notString() {
		return new self('parameter is not a string');
	}
}