<?php
namespace P3\MongoExt\Search;

/**
 * @author Samuel Carlier
 */
class Query {

	/**
	 * @var int
	 */
	private $limit = 100;

	/**
	 * @var int
	 */
	private $skip = 0;

	/**
	 * @var string
	 */
	private $category;

	/**
	 * @var string
	 */
	private $language;

	/**
	 * @var array
	 */
	private $identifier = array();

	/**
	 * @var array
	 */
	private $keywords = array();
	
	/**
	 * @var array
	 */
	private $searchAll = array();
	
	/**
	 * @var \DateTime
	 */
	private $dateStart = array();

	/**
	 * @var \DateTime
	 */
	private $dateEnd = array();

	/**
	 * @var bool
	 */
	private $publish = false;
	
	/**
	 * @param  string $name
	 * @param  string $string
	 * @return self
	 */
	public function keywords($name, $string) {
		$this->keywords[$name] = array();

		$string = preg_replace('/[^a-z0-9]/i', ' ', $string);
		$string = preg_replace('/\s+/', ' ', $string);

		$stringParts = explode(' ', $string);
		foreach($stringParts as $k => $value) {
			$stringParts[$k] = strtolower(trim($value));
		}

		$stringParts = array_unique($stringParts);

		while(list(,$value) = each($stringParts)) {
			$this->keywords[$name][] = $value;
		}
		
		return $this;
	}
	
	/**
	 * @param  string $string
	 * @return self
	 */
	public function searchAll($string) {
		$string = preg_replace('/[^a-z0-9]/i', ' ', $string);
		$string = preg_replace('/\s+/', ' ', $string);

		$string = explode(' ', $string);
		while(list(,$value) = each($string)) {
			$this->searchAll[] = strtolower(trim($value));
		}
		
		$this->searchAll = array_unique($this->searchAll);

		return $this;
	}
	
	/**
	 * @param  \DateTime $date
	 * @return self
	 */
	public function setDateStart(\DateTime $date) {
		$this->dateStart = $date;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateStart() {
		return $this->dateStart;
	}
	
	/**
	 * @param  \DateTime $date
	 * @return self
	 */
	public function setDateEnd(\DateTime $date) {
		$this->dateEnd = $date;
		return $this;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getDateEnd() {
		return $this->dateEnd;
	}

	/**
	 * shortcut to make start/end date behave as a publication
	 * where you only get a result between "current date" and "dateEnd"
	 * 
	 * @param  bool $bool
	 * @return self
	 */
	public function setDateAsPublish($bool=true) {
		$this->publish = true;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function isDateAsPublish() {
		return $this->publish;
	}
	
	/**
	 * @return array
	 */
	public function getKeywords() {
		return $this->keywords;
	}
	
	/**
	 * @return array
	 */
	public function getSearchAll() {
		return $this->searchAll;
	}

	/**
	 * @param  string $language
	 * @return self
	 */
	public function language($language) {
		$this->language = $language;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @param  int $limit
	 * @return self
	 */
	public function limit($limit) {
		$this->limit = (int) $limit;
		return $this;
	}

	/**
	 * @param  array $identifier
	 * @return self
	 */
	public function identifier($identifier) {
		$this->identifier = $identifier;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getIdentifier() {
		return $this->identifier;
	}

	/**
	 * @return int
	 */
	public function getLimit() {
		return $this->limit;
	}

	/**
	 * @param  int $skip
	 * @return self
	 */
	public function skip($skip) {
		$this->skip = $skip;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSkip() {
		return $this->skip;
	}

	/**
	 * @param  string $category
	 * @return self
	 */
	public function category($category) {
		$this->category = $category;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}
}