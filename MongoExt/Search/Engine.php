<?php
namespace P3\MongoExt\Search;

/**
 * @author Samuel Carlier
 */
class Engine {

	/**
	 * @var \P3\Mongo\DB
	 */
	private static $staticConnection;

	/**
	 * @var \P3\Mongo\DB
	 */
	private $connection;

	/**
	 * @var string
	 */
	private static $staticCollection = '\P3\MongoExt\Search\Document';

	/**
	 * @var string
	 */
	private $collection;

	/**
	 * just a method so its easier to execute a single command
	 *
	 * @param \P3\Mongo\DB $connection
	 * @param string $collection
	 * 
	 * @return self
	 */
	public static function instance($connection=null, $collection=null) {
		return new self($connection, $collection);
	}
	
	/**
	 * @param \P3\Mongo\DB $connection
	 * @param string $collection
	 */
	public function __construct($connection=null, $collection=null) {
		$this->connection = $connection;
		$this->collection = $collection;

		$this->checkSettings();
	}

	/**
	 * @param \P3\Mongo\DB $connection
	 * @param string	   $collection
	 */
	public static function setSettings(\P3\Mongo\DB $connection, $collection=null) {
		self::$staticConnection = $connection;

		if($collection) {
			self::$staticCollection = $collection;
		}
	}

	/**
	 *
	 */
	private function checkSettings() {
		$collection = $this->collection ?: self::$staticCollection;
		$connection = $this->connection ?: self::$staticConnection;

		if(!is_string($collection)) {
			throw Exception::invalidCollection();
		}

		if(!$connection instanceof \P3\Mongo\DB) {
			throw Exception::invalidConnection();
		}		
	}

	/**
	 * @return \P3\Mongo\DB
	 */
	private function getConnection() {
		return $this->connection ?: self::$staticConnection;
	}

	/**
	 * @return \P3\Mongo\Collection
	 */
	private function getCollection() {
		$collection = $this->collection ?: self::$staticCollection;
		$connection = $this->connection ?: self::$staticConnection;

		return $connection->getCollection($collection);
	}

	/**
	 * @param  \P3\MongoExt\Search\Document $document
	 * @return self
	 */
	public function save(\P3\MongoExt\Search\Document $document) {
		$this->deleteByNameLanguage($document->getName(), $document->getLanguage());
		$this->getConnection()->save($document);

		return $this;
	}

	/**
	 * count will go beyond 1000 for performance reason
	 * if count is 1001 it means it can be 1001 to 100000 or more
	 *
	 * @param  \P3\MongoExt\Search\Query $query
	 * @return array
	 */
	public function find(\P3\MongoExt\Search\Query $query) {
		$find = array();

		//named search maken!
		if($query->getSearchAll()) {
			$words = $query->getSearchAll();

			foreach($words as $k => $value) {
				$words[$k] = new \MongoRegex('/^' . $value . '/');
			}

			$find['searchAll'] = array('$all' => $words);

		} elseif($query->getKeywords()) {
			$keywords = $query->getKeywords();

			$words = array();
			foreach($keywords as $key => $value) {
				foreach($value as $k => $v) {
					$words[] = new \MongoRegex('/^' . md5($key) . ':' . $v . '/');
				}
			}

			$find['search'] = array('$all' => $words);
		}

		if($query->getIdentifier()) {
			$find['identifier'] = array('$all' => $query->getIdentifier());
		}

		if($query->getCategory()) {
			$find['category'] = $query->getCategory();
		}

		if($query->getLanguage()) {
			$find['language'] = $query->getLanguage();
		}

		if($query->isDateAsPublish()) {
			$find['dateStart'] = array('$lt' => new \MongoDate);
			$find['dateEnd']   = array('$gt' => new \MongoDate);
		}

		$cursor = $this->getCollection()->find($find);
		$cursor->limit($query->getLimit())->skip($query->getSkip());

		$count = $cursor->count(true);
		if($count == $query->getLimit()) {
			$count = $this->getCollection()->find($find)->fields(array("_id" => true))->limit(1001)->count();
		}

		$returnset = array();
		$returnset['cursor'] = $cursor;
		$returnset['count']  = $count;

		return $returnset;
	}

	/**
	 * @param string $category
	 */
	public function deleteByCategory($category) {
		if(!is_string($category)) {
			throw Exception::notString();
		}

		$this->getCollection()->remove(array('category' => $category));
	}

	/**
	 * it will delete all documents matching the values of identifier
	 * the document must have the same identifier or the same and more
	 *
	 * @param array  $identifier
	 * @param string $category
	 */
	public function deleteByIdentifierCategory(array $identifier, $category) {
		if(!is_string($category)) {
			throw Exception::notString();
		}

		$find = array();
		while(list(,$value) = each($identifier)) {
			if(!is_string($value)) {
				throw Exception::notString();
			}

			$find[] = $value;
		}

		$this->getCollection()->remove(array('category' => $category, 'identifier' => array('$all' => $find)));
	}
	
	/**
	 * it will delete all documents matching the values of identifier
	 * the document must have the same identifier or the same and more
	 *
	 * @param array $identifier
	 */
	public function deleteByIdentifier(array $identifier) {
		$find = array();
		while(list(,$value) = each($identifier)) {
			if(!is_string($value)) {
				throw Exception::notString();
			}

			$find[] = $value;
		}

		$this->getCollection()->remove(array('identifier' => array('$all' => $find)));
	}

	/**
	 * @param array $name
	 */
	public function deleteByName($name) {
		if(!is_string($name)) {
			throw Exception::notString();
		}

		$this->getCollection()->remove(array('name' => $name));
	}
	
	/**
	 * @param array $name
	 * @param array $language
	 */
	public function deleteByNameLanguage($name, $language) {
		if(!is_string($name) || !is_string($language)) {
			throw Exception::notString();
		}

		$this->getCollection()->remove(array('name' => $name, 'language' => $language));
	}
}