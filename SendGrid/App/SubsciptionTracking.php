<?php
namespace P3\SendGrid\App;

/**
 * @author Samuel Carlier
 */
class SubsciptionTracking implements AppInterface {

	/**
	 * @var bool
	 */
	private $enable;

	/**
	 * @var string
	 */
	private $htmlLink;

	/**
	 * @var string
	 */
	private $plainLink;

	/**
	 * @var string
	 */
	private $landingPage;

	/**
	 * @param bool $enable
	 */
	public function __construct($enable=true) {
		$this->enable = $enable === true ? true : false;
	}

	/**
	 * @param  string $link
	 * @return self
	 */
	public function setLink($link) {
		$this->htmlLink  = $link;
		$this->plainLink = strip_tags($link);

		return $this;
	}

	/**
	 * @param  string $landingPageUrl
	 * @return self
	 */
	public function setLandingPage($landingPageUrl) {
		$this->landingPage = $landingPageUrl;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAsArray() {
		$settings = array('enable'	   => $this->enable ? 1 : 0,
						  'text/html'  => $this->htmlLink,
						  'text/plain' => $this->plainLink,
						  'landing'	   => $this->landingPage,
						  'url'		   => 'http://www.araneum.nl'
		);
		return array('subscriptiontrack' => array('settings' => $settings));
	}

}