<?php
namespace P3\SendGrid\App;

/**
 * @author Samuel Carlier
 */
interface AppInterface {

	/**
	 * @return array
	 */
	public function getAsArray();

}