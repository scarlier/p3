<?php
namespace P3\SendGrid\App;

/**
 * @author Samuel Carlier
 */
class ClickTracking implements AppInterface {

	/**
	 * @var bool
	 */
	private $enable;

	/**
	 * @param bool $enable
	 */
	public function __construct($enable=true) {
		$this->enable = $enable === true ? true : false;
	}

	/**
	 * @return array
	 */
	public function getAsArray() {
		$settings = array('enable' => $this->enable ? 1 : 0);
		return array('clicktrack' => array('settings' => $settings));
	}

}