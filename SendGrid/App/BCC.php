<?php
namespace P3\SendGrid\App;

/**
 * @author Samuel Carlier
 */
class BCC implements AppInterface {

	/**
	 * @var array
	 */
	private $emails = array();

	/**
	 * @param string $email
	 */
	public function __construct($email='') {
		if($email) {
			foreach(explode(',', $email) as $e) {
				$this->addEmail($e);
			}
		}
	}

	/**
	 * @param string $email
	 */
	public function addEmail($email) {
		$this->emails[] = $email;
	}

	/**
	 * @return array
	 */
	public function getAsArray() {
		$settings = array('email' => $this->emails[0]);
		return array('bcc' => array('settings' => $settings));
	}

}