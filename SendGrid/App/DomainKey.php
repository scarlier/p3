<?php
namespace P3\SendGrid\App;

/**
 * @author Samuel Carlier
 */
class DomainKey implements AppInterface {

	/**
	 * @var bool
	 */
	private $enable;

	/**
	 * @var string
	 */
	private $domain;

	/**
	 * @param bool   $enable
	 * @param string $domain
	 */
	public function __construct($enable, $domain) {
		$this->enable = $enable === true ? true : false;
		$this->domain = $domain;
	}

	/**
	 * @return array
	 */
	public function getAsArray() {
		$settings = array('enable' => $this->enable ? 1 : 0,
						  'domain' => $this->domain,
						  'sender' => 1
		);
		return array('domainkeys' => array('settings' => $settings));
	}

}