<?php
namespace P3\SendGrid\Api;

/**
 * @author Samuel Carlier
 */
class SubUsers extends Base {

	/**
	 * 
	 */
	const URL = 'https://sendgrid.com/api/';

	/**
	 * @param string $username
	 *
	 * @return  array
	 *			false if nothing found
	 */
	public function retrieve($username) {
		$data = array('username' => $username, 'task' => 'get');
		$returnset = $this->send($data, self::URL . 'user.profile.xml');
		if(empty($returnset['user'])) {
			return false;
		}

		return $returnset['user'];
	}

	/**
	 * @param string $username
	 * @return bool
	 */
	public function disable($username) {
		$data = array('user' => $username);
		return $this->send($data, self::URL . 'user.disable.xml');
	}

	/**
	 * @param string $username
	 * @return bool
	 */
	public function enable($username) {
		$data = array('user' => $username);
		return $this->send($data, self::URL . 'user.enable.xml');
	}

	/**
	 * @param \P3\Sendgrid\SubUser $user
	 * 
	 * @return true if success
	 *		   array if failed
	 */
	public function create(\P3\Sendgrid\SubUser $user) {
		$data = array('username'	=> $user->getUsername(),
					  'password'	=> $user->getPassword(),
					  'email'		=> $user->getEmail(),
					  'first_name'	=> $user->getFirstName(),
					  'last_name'	=> $user->getLastName(),
					  'address'		=> $user->getAddress(),
					  'city'		=> $user->getCity(),
					  'state'		=> $user->getState(),
					  'zip'			=> $user->getZip(),
					  'country'		=> $user->getCountry(),
					  'phone'		=> $user->getPhone(),
					  'website'		=> $user->getWebsite(),
					  'mail_domain'	=> $user->getMailDomain(),
					  'confirm_password' => $user->getPassword()
		);

		$returnset = $this->send($data, self::URL . 'user.add.xml');
		if($returnset === true) {
			return true;
		}

		return $returnset;
	}

}