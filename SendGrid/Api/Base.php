<?php
namespace P3\SendGrid\Api;

/**
 * @author Samuel Carlier
 */
abstract class Base {

	/**
	 * @var string
	 */
	private $apiKey;

	/**
	 * @var string
	 */
	private $apiUser;

	/**
	 * @param string $user
	 * @param string $key
	 */
	public function __construct($user, $key) {
		$this->apiUser = $user;
		$this->apiKey  = $key;
	}

	/**
	 * @param array  $data
	 * @param string $url
	 *
	 * @return array
	 *		  false if failed
	 */
	protected function send(array $data, $url=null) {
		$data = array_merge(array('api_user' => $this->apiUser), $data);
		$data = array_merge(array('api_key' => $this->apiKey), $data);

		$params = array('http' => array(
			'method' => 'POST',
			'content' => $this->build_post_query($data),
			'header' =>  "Content-type: application/x-www-form-urlencoded\r\n"
		));

		$ctx = stream_context_create($params);
		$fp = @fopen($url ?: $this::URL, 'rb', false, $ctx);

		if(!$fp) {
			return false;
		}

		$response = @stream_get_contents($fp);
		fclose($fp);

		if($response === false) {
			return false;
		}

		$xml = simplexml_load_string($response);

		//general result response
		if($xml->message) {
			if((string) $xml->message == 'success') {
				return true;
			}

			return json_decode(json_encode($xml), true);
		}

		//array response
		return json_decode(json_encode($xml), true);
	}

	/**
	 * @param array  $a
	 * @param string $b
	 * @param int    $c
	 *
	 * @return string
	 */
	private function build_post_query($a, $b='', $c=0) {
		if(!is_array($a)) {
			return false;
		}

		foreach($a as $k => $v) {
			if($c) {
				if(!is_numeric($k)) {
					$k = $b . '['.$k.']';
				} else {
					$k = $b . '[]';
				}
			}

			if(is_array($v)) {
				$r[] = $this->build_post_query($v, $k, 1);
				continue;
			}

			$r[] = urlencode($k). "=" . urlencode($v);
		}

		return implode("&", $r);
	}

}