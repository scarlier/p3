<?php
namespace P3\SendGrid\Api;

/**
 * @author Samuel Carlier
 */
class Stats extends Base {

	/**
	 * 
	 */
	const URL = 'https://sendgrid.com/api/stats.get.xml';

	/**
	 * @param int $days Must be an integer greater than 0
	 * @param string $start_date Date must be in YYYY-mm-dd format
	 * @param string $end_date   Date must be in YYYY-mm-dd format
	 *
	 * @return array
	 *		   false if not success
	 */
	public function retrieveStatistics($days, $start_date=null, $end_date=null) {
		$data = array();

		if($days) {
			$data['days'] = $days;
		}

		if($start_date) {
			$data['start_date'] = $start_date;
		}

		if($end_date) {
			$data['end_date'] = $end_date;
		}

		if(empty($data)) {
			return false;
		}

		return $this->send($data);
	}

	/**
	 * Retrieve all-time totals for your subusers
	 *
	 * @return array
	 *		   false if not success
	 */
	public function retrieveAggregates() {
		$data = array('aggregate' => 1);
		return $this->send($data);
	}

	/**
	 * Retrieve a list of all the categories used in your account.
	 *
	 * @return array
	 *		   false if not success
	 */
	public function categoryList() {
		$data = array('list' => true);
		return $this->send($data);

	}
}