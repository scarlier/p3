<?php
namespace P3\SendGrid;

/**
 * @author Samuel Carlier
 */
class Header {

	/**
	 * @var array
	 */
	private $to = array();
	
	/**
	 * @var array
	 */
	private $properties = array();
	
	/**
	 * @var array
	 */
	private $foundProperties = array();
	
	/**
	 * @var array
	 */
	private $apps = array();

	/**
	 * @var string
	 */
	private $category;

	/**
	 * @var string
	 */
	static private $categoryPrefix;

	/**
	 * @param  string $email
	 * @param  array  $properties
	 * 
	 * @return self
	 */
	public function addTo($email, array $properties = array()) {
		$email = strtolower($email);
		$this->to[$email] = $properties;

		foreach($properties as $k => $v) {
			$this->foundProperties[trim($k, '-')] = true;
		}

		return $this;
	}
	
	/**
	 * @return int
	 */
	public function countTo() {
		return count($this->to);
	}
	
	/**
	 * @param  \P3\SendGrid\App\AppInterface $app
	 * @return self
	 */
	public function addApp(\P3\SendGrid\App\AppInterface $app) {
		$this->apps[] = $app;
		return $this;
	}

	/**
	 * @param string $categoryPrefix
	 */
	public static function setCategoryPrefix($categoryPrefix) {
		self::$categoryPrefix = $categoryPrefix;
	}

	/**
	 * @param  string $category
	 * @return self
	 */
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}

	/**
	 * @return string a json string
	 */
	public function create() {
		$header = array();

		if($this->apps) {
			$header['filters'] = $this->getApps();
		}

		if(($category = $this->getCategory()) !== null) {
			$header['category'] = $category;
		}

		if($this->to) {
			if($this->foundProperties) {
				$header['sub'] = array();

				foreach($this->foundProperties as $k => $v) {
					$header['sub']['-' . $k . '-'] = array();					
				}
			}

			foreach($this->to as $to => $properties) {
				$header['to'][] = $to;
				
				if(!$this->foundProperties) {
					continue;
				}
				
				if(!is_array($properties)) {
					$properties = array();
				}
				
				foreach($this->foundProperties as $k => $v) {
					if(!array_key_exists($k, $properties)) {
						$properties[$k] = '';
					}

					$header['sub']['-' . $k . '-'][] = $properties[$k];
				}
			}
		}

		
		// Add spaces so that the field can be foldd
		return preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', json_encode($header));
	}

	/**
	 * @return array
	 */
	private function getApps() {
		$filters = array();
		foreach($this->apps as $app) {
			$filters = array_merge($app->getAsArray(), $filters);
		}

		return $filters;
	}

	/**
	 * @return string
	 *		   null if empty
	 */
	private function getCategory() {
		$category = '';

		if(self::$categoryPrefix) {
			$category .= self::$categoryPrefix;
		}

		if($this->category) {
			$category .= $this->category;
		}

		if(empty($category)) {
			return null;
		}

		return $category;
	}
}